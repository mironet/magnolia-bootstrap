# Magnolia bootstrapper and JCR manipulator

Bootstrap Magnolia CMS in a different way than JCR exports/bootstrap files.

[[_TOC_]]

## Note

This repository is part of an ongoing effort to containerize
[Magnolia CMS](https://www.magnolia-cms.com/).

## Usage

### Init Container

This program can be used as a one-off job (init container) in Kubernetes to
apply final configuration to a new
[Magnolia CMS deployment via Helm](https://gitlab.com/mironet/magnolia-helm).

### Sidecar

It could also be used in "server mode" (as a sidecar). In this mode it also does
regular health checks against Magnolia and is intended to be the health check
target of Kubernetes as opposed to directly monitor Magnolia.

## How it works

### Server mode

In server mode this program mainly does 3 initial tasks when run as a sidecar to
a magnolia public and an additional 4th job when run as a sidecar to a magnolia
author.

These initial tasks are to

- set the default base URL
- set the superuser password
- set the public key required for publishing
- and (for author sidecars) set the public receivers.

All this is done without using instruction yaml files (as it used to be the case
in older versions of this program).

### Boot mode

In boot mode this program reads a YAML file at the given location (see `-i`) and then
generates API calls towards the
["REST" endpoints of Magnolia CMS](https://documentation.magnolia-cms.com/display/DOCS61/Nodes+endpoint+API).

### Structure of the YAML file

The instructions are executed in order. Each instruction can be executed against
one or more targets, called "endpoints":

```yaml
endpoints:
  - name: magnolia-public
    url: http://localhost:8080/
  - name: magnolia-author
    url: http://author.svc.cluster.local:8080/
```

Instructions are all listed as an array named `playbook`:

```yaml
playbook:
  - name: remove_sub
    endpoints:
      - magnolia-author
    ...

  - name: add_new_subs
    endpoints:
      - magnolia-author
    ...
```

### Reference

This is a list of all fields defined in an instruction and its usage.

#### Instruction

| Name | Usage |
|-|-|
| `name` | Name of the instruction, required. |
| `method` | HTTP method used in this instruction. |
| `host` | Host in case we want to switch hosts in between executions. |
| `endpoints` | List of endpoints (hosts) defined in the instruction list. |
| `username` | Override username for this request. |
| `password` | Override password for this request. |
| `bearer_token` | Set the Authorization bearer token to this value. Overrides basic auth (username, password). |
| `path` | The target path. |
| `force` | Force update/delete, do not check for idempotency. |
| `allow_failure` | Allow this instruction to fail. |
| `wait_for` | Wait this long for the command to succeed/fail. |
| `retry_on` | Retry on those HTTP return codes. Other codes outside of 200-399 are considered as errors. |
| `retry` | Retry this many times. |
| `data` | The instruction data. |
| `register` | Register output from this instruction to a var with this name. Not yet implemented! |
| `when` | If this go template expression evaluates to true, this instruction is executed. Not yet implemented! |

#### InstructionList

| Name | Usage |
|-|-|
| `endpoints` | Map of endpoint name to URL. |
| `playbook` | Our list of instructions. |

#### Endpoints

| Name | Usage |
|-|-|

#### Endpoint

| Name | Usage |
|-|-|
| `name` | The name of the endpoint. |
| `url` | The url of the endpoint. |

### Templating

You can use Go templating in the YAML file since it's first processed by the
template engine. For example this `bcrypt` function, hashing a password.

```text
{{ bcrypt "supersecretpassword1234"| quote }}
```

You can also use env vars present to the process (container) when running, like
so:

```yaml
...
    properties:
      - name: test
        type: String
        values: [
          - {{ .Env.HOME | quote }}
        ]
```

This would result in:

```yaml
properties:
  - name: test
    type: String
    values: ["/home/user"]
```

There's also the `default` function if an env var is not set:

```text
{{ .Env.EMPTYVAR | default "non-empty-default-value" }}
```

This would result in `non-empty-default-value` if `EMPTYVAR` is not set.

You can test the results with:

```bash
./mgnlboot template -i instructions.yml
```

You can also read in files from anywhere in the filesystem through the `.File` struct in templates like this:

```text
{{ .File.Get "/path/to/file.txt" }}
```

And generate signed JWT tokens with claims in them:

```text
{{ genjwt "{ \"name\": \"test\" }" (.File.Get "/path/to/private-key.pem") }}
```

When using this program on a Kubernetes cluster, you can also create a JWT
token (containing all standard claims required for requests to magnolia) that is
signed by a private key which is contained in a k8s secret. The referenced
Kubernetes secret is expected to be located in the same namespace as the
bootstrapper and to have a data key `private.pem` under which the private key is
stored in pem format.

```text
{{ genjwtFromSecret "<name of the secret containing the private key>" }}
```

e.g.

```text
{{ genjwtFromSecret "magnolia-auth-key" }}
```

### Example

Find an example in the `example.yml` file [here](example/example.yml).

### Execution

The tool is designed to be idempotent. You should be able to repeatedly execute
the same instruction file and get the same result each time.

```bash
./mgnlboot boot -i cmd/testdata/fixtures/exampleInstructionList.yml -m https://magnolia.example.com/author --username <username> --password <password>
```

You can use env vars for all flags the binary takes. For example for the
username/password you could do this:

```bash
export MGNLBOOT_USERNAME=admin
# The space at the beginning prevents logging this command in the shell history.
 export MGNLBOOT_PASSWORD=supersecretpass
export MGNLBOOT_MAGNOLIA=https://magnolia.example.com/author

./mgnlboot boot -i cmd/testdata/fixtures/exampleInstructionList.yml
```

### Server Mode

In server mode you can specify HTTP code ranges which are considered "healthy"
responses, like this:

```bash
./mgnlboot serve -m http://localhost:8080 -i instructions.yml -vv --code 200-399,401,404
```

This way the internal Magnolia health check is considered successful if Magnolia
responds with any of those HTTP response codes above: `200-399,401,404`

Also regular expressions (the [Go
variant](https://golang.org/pkg/regexp/syntax/)) can be used to only claim
success in case the expression matches the returned body. HTTP headers are not
checked against regexp.

```bash
./mgnlboot serve -m http://localhost:8080 -i instructions.yml -vv --response "Welcome to Magnolia" --code 200-202
```

In case the response contains the string `Welcome to Magnolia` and the response
code is between 200 and 202, the healthcheck is considered successful.

The server exposes (as a default) an HTTP endpoint on localhost:8765 intended
for Kubernetes to do readiness and liveness checks against:

<http://localhost:8765/healthz>

This returns success in case Magnolia itself is healthy _and_ all instructions
have been processed successfully. In case no instructions were given, only
Magnolia itself is relevant for this check.

### Set superuser password using request to bootstrap server

```bash
curl -v -d 'user=superuser&pw=myOwnPassword' -X POST "http://localhost:8765/superuser-password"
```

### Reenable superuser using request to bootstrap server

```bash
curl -v -d 'user=superuser' -X POST "http://localhost:8765/reenable-superuser"
```

## Do not delete public receiver nodes automatically

Set the flag `keep-receiver-nodes` if the bootstrapper should not delete any
public receiver nodes automatically. Note: This means that the public receivers
must always be deleted manually.

Instead of setting the flag, one can also set environment variable
`MGNLBOOT_KEEP_RECEIVER_NODES` to `true`.

## Do not create public receiver nodes automatically

Set the flag `deactivate-receiver-creation` if the bootstrapper should not
create any public receiver nodes automatically. Note: This means that the public
receivers must always be created manually.

Instead of setting the flag, one can also set environment variable
`MGNLBOOT_DEACTIVATE_RECEIVER_CREATION` to `true`.

## Multicluster (with linkerd mirrored services)

In a multicluster scenario with public instance services that are mirrored from
another cluster using [linkerd](https://linkerd.io/), one can use environment
variable `MGNLBOOT_MULTI_PUBLIC_SERVICE_NAMES` to specify service names. If
there is more than one such service name specified in the env var, use `,` to
separate the different service names. The bootstrapper will then look for
endpoints which have the label `mirror.linkerd.io/headless-mirror-svc-name` and
a key which is one of the service names in the list. For each such endpoint a
public receiver node is added to the magnolia author by the bootstrapper.

## Status

This project is still beta, see the
[release page](https://gitlab.com/mironet/magnolia-bootstrap/-/releases) for
binary releases.

## Legal Notes

Magnolia, Magnolia Blossom, Magnolia and the Magnolia logo are registered
trademark or trademarks of Magnolia International Ltd.
