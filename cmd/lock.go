package cmd

import (
	"sync"
	"time"
)

var defaultLockTimeout = time.Second * 30

// ActivationLock is a map holding locks for the activation lock feature.
type ActivationLock struct {
	mux   sync.Mutex
	locks map[string]*time.Timer
}

// NewActivationLock returns a new lock collection.
func NewActivationLock() *ActivationLock {
	a := &ActivationLock{
		locks: make(map[string]*time.Timer),
	}
	return a
}

// Lock a named lock and return its duration until auto-unlock. After the lock
// has been auto-unlocked, callback is called.
func (a *ActivationLock) Lock(name string, callback func()) time.Duration {
	d := defaultLockTimeout
	a.mux.Lock()
	defer a.mux.Unlock()
	t, ok := a.locks[name]
	if !ok {
		t = time.AfterFunc(d, func() {
			// Remove own timer after it has stopped.
			a.mux.Lock()
			defer a.mux.Unlock()
			delete(a.locks, name)
			if callback != nil {
				callback()
			}
		})
		a.locks[name] = t
		return d
	}
	// Stop the timer and reset without firing the func.
	if !t.Stop() {
		<-t.C
	}
	t.Reset(d)
	return d
}

// Unlock unlocks a named lock and returns true. If it does not exist, nothing
// happens and false is returned.
func (a *ActivationLock) Unlock(name string) bool {
	a.mux.Lock()
	defer a.mux.Unlock()
	t, ok := a.locks[name]
	if !ok {
		return false
	}
	// Stop the timer and drain the channel so it can be garbage collected.
	if !t.Stop() {
		<-t.C
	}
	// Then delete the timer from the map.
	delete(a.locks, name)
	return true
}

// IsLocked reports true if more than 0 locks are locked in the collection.
func (a *ActivationLock) IsLocked() bool {
	a.mux.Lock()
	defer a.mux.Unlock()
	return len(a.locks) > 0
}
