package cmd

import (
	"bytes"
	"io"
	"testing"
)

func Test_getInstructionList(t *testing.T) {
	bb := fixture("templateEnvTest.yml", t)
	buf := bytes.NewReader([]byte(bb))

	type wantFunc func(*InstructionList) error

	type args struct {
		in io.Reader
	}
	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "simple template parse",
			args: args{
				in: buf,
			},
			want: func(list *InstructionList) error {
				return nil // TODO: fix.
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, _ = buf.Seek(0, 0)
			var list InstructionList
			if err := getInstructionList(&list, tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("getInstructionList() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.want != nil {
				if err := tt.want(&list); err != nil {
					t.Error(err)
				}
			}
			t.Log(list)
		})
	}
}
