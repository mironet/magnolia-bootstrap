package cmd

import (
	"fmt"
	"io"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	//"gopkg.in/yaml.v3"
)

// templateCmd represents the template command
var templateCmd = &cobra.Command{
	Use:   "template",
	Short: "Print the result of template processing.",
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["template"]
		infile := vip.GetString("input")
		if infile == "" {
			return fmt.Errorf("no template file provided")
		}
		outfile := vip.GetString("output")
		if outfile == "" {
			return fmt.Errorf("no output file provided")
		}
		_ = setVerbosity(vip.GetInt("verbosity"))

		var input io.Reader
		switch infile {
		case "-":
			input = os.Stdin
		default:
			// Get content of input file.
			file, err := os.Open(infile)
			if err != nil {
				return fmt.Errorf("could not open template file: %w", err)
			}
			defer file.Close()
			input = file
		}

		var output io.Writer
		switch outfile {
		case "-":
			output = os.Stdout
		default:
			file, err := os.Create(outfile)
			if err != nil {
				return fmt.Errorf("could not create destination file: %w", err)
			}
			defer func() {
				err := file.Close()
				if err != nil {
					logrus.Error(err)
					return
				}
				logrus.Infof("template from %s written to %s", infile, outfile)
			}()
			switch {
			case logrus.IsLevelEnabled(logrus.TraceLevel):
				output = io.MultiWriter(os.Stderr, file)
			default:
				output = file
			}
		}

		if err := templateThis(output, input); err != nil {
			logrus.Errorf("could not execute template: %s", err)
			return nil
		}

		return nil
	},
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["template"] = v

	flags := templateCmd.Flags()

	flags.StringP("input", "i", "-", "File input to use for template processing.")
	flags.StringP("output", "o", "-", "Where to put the processed template.")
	flags.CountP("verbosity", "v", "Verbosity level (--v: verbose, --vv: debug, --vvv: trace)")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(templateCmd)
}
