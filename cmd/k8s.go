package cmd

import (
	"context"
	"fmt"
	"os"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type k8sCli struct {
	cliSet *kubernetes.Clientset

	instanceType  string  // The instance type of this bootstrapper's magnolia instance.
	releaseName   string  // The release name of this bootstrapper's magnolia instance.
	namespaceName string  // The namespace name of this bootstrapper's magnolia instance.
	podName       string  // The pod name of this bootstrapper's magnolia instance.
	serviceName   string  // The name of the service that serves public instances.
	pod           *v1.Pod // The pod of this bootstrapper's magnolia instance.
}

const (
	instanceTypeAuthor            = "author-instance"
	instanceTypePublic            = "public-instance"
	serviceAccountNamespace       = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
	hostnameEnvVar                = "HOSTNAME"
	publicServiceNameEnvVar       = "MGNLBOOT_PUBLIC_SERVICE_NAME"
	multiPublicServiceNamesEnvVar = "MGNLBOOT_MULTI_PUBLIC_SERVICE_NAMES"
	superuserNameEnvVar           = "MGNLBOOT_SUPERUSER_NAME"
	defaultBaseUrlEnvVar          = "MGNL_DEFAULT_BASE_URL"
	labelKeyApp                   = "app"
	labelKeyChart                 = "chart"
	labelKeyHeritage              = "heritage"
	labelKeyComponent             = "component"
	labelKeyRelease               = "release"
	labelKeyTier                  = "tier"
	appLabelValueTier             = "app"

	labelKeyHeadlessMirror = "mirror.linkerd.io/headless-mirror-svc-name"

	labelKeyMicroprofileConfig   = "magnolia.info/microprofile-config"
	dataKeyAuthorMicroprofConfig = "microprofile-config-author.properties"
)

// k8sClient creates a kubernetes client and uses it to fill the k8s struct
// (which contains information on the kubernetes environment this server runs
// in).
func k8sClient(ctx context.Context) (*k8sCli, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("could not create in-cluster config: %w", err)
	}
	k8s := &k8sCli{}
	k8s.cliSet, err = kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("could not create new clientset: %w", err)
	}
	err = k8s.fill(ctx)
	if err != nil {
		return nil, fmt.Errorf("could not fill k8s struct: %w", err)
	}
	return k8s, nil
}

func (cli *k8sCli) superuserSecretName() (string, error) {
	switch cli.instanceType {
	case instanceTypeAuthor:
		return fmt.Sprintf("%s-secret-superuser-author", cli.releaseName), nil
	case instanceTypePublic:
		return fmt.Sprintf("%s-secret-superuser-public", cli.releaseName), nil
	default:
		return "", fmt.Errorf("encounterd unknown instance type %s", cli.instanceType)
	}
}

func (cli *k8sCli) fill(ctx context.Context) error {
	// Fill pod name.
	cli.podName = os.Getenv(hostnameEnvVar)
	if cli.podName == "" {
		return fmt.Errorf("environment variable %s must be set", hostnameEnvVar)
	}

	// Fill service name.
	cli.serviceName = os.Getenv(publicServiceNameEnvVar)
	if cli.serviceName == "" {
		return fmt.Errorf("environment variable %s must be set", publicServiceNameEnvVar)
	}

	// Fill namespace name.
	dat, err := os.ReadFile(serviceAccountNamespace)
	if err != nil {
		return fmt.Errorf("could not read namesapce file: %w", err)
	}
	cli.namespaceName = string(dat)

	// Fill pod.
	cli.pod, err = cli.cliSet.CoreV1().Pods(cli.namespaceName).Get(ctx, cli.podName, metav1.GetOptions{})
	if err != nil {
		return fmt.Errorf("could not get pod the bootstrapper is running in: %w", err)
	}

	var ok bool
	cli.instanceType, ok = cli.pod.Labels[labelKeyComponent]
	if !ok {
		return fmt.Errorf("expected pod to have label key %q, but has not", labelKeyComponent)
	}

	cli.releaseName, ok = cli.pod.Labels[labelKeyRelease]
	if !ok {
		return fmt.Errorf("expected pod to have label key %q, but has not", labelKeyRelease)
	}
	return nil
}

func (cli *k8sCli) microprofileConfigMap(ctx context.Context) (v1.ConfigMap, error) {
	if cli.pod == nil {
		return v1.ConfigMap{}, fmt.Errorf("bootstrapper pod pointer cannot be nil")
	}
	selector := labels.SelectorFromSet(map[string]string{
		labelKeyApp:                cli.pod.Labels[labelKeyApp],
		labelKeyChart:              cli.pod.Labels[labelKeyChart],
		labelKeyRelease:            cli.pod.Labels[labelKeyRelease],
		labelKeyHeritage:           cli.pod.Labels[labelKeyHeritage],
		labelKeyMicroprofileConfig: "true",
	}).String()
	configMapList, err := cli.cliSet.CoreV1().ConfigMaps(cli.namespaceName).List(ctx, metav1.ListOptions{
		LabelSelector: selector,
	})
	if err != nil {
		return v1.ConfigMap{}, fmt.Errorf("could not list config maps: %w", err)
	}
	if configMapList == nil {
		return v1.ConfigMap{}, fmt.Errorf("config map list pointer cannot be nil")
	}

	switch len(configMapList.Items) {
	case 0:
		return v1.ConfigMap{}, fmt.Errorf("no microprofile config map found")
	case 1:
		return configMapList.Items[0], nil
	default:
		return v1.ConfigMap{}, fmt.Errorf("found more than one microprofile config map")
	}
}

func (cli *k8sCli) publicEndpoints(ctx context.Context) (v1.Endpoints, error) {
	endpoints, err := cli.cliSet.CoreV1().Endpoints(cli.namespaceName).Get(ctx, cli.serviceName, metav1.GetOptions{})
	if err != nil {
		return v1.Endpoints{}, fmt.Errorf("could not get service %s: %w", cli.serviceName, err)
	}
	if endpoints == nil {
		return v1.Endpoints{}, fmt.Errorf("service %s pointer cannot be nil", cli.serviceName)
	}
	return *endpoints, nil
}

func (cli *k8sCli) remoteServiceList(ctx context.Context) ([]v1.Service, error) {
	out := make([]v1.Service, 0)
	env := os.Getenv(multiPublicServiceNamesEnvVar)
	if env == "" {
		logrus.Debugf("env var %s not set", multiPublicServiceNamesEnvVar)
		return out, nil
	}
	svcNames := strings.Split(env, ",")
	logrus.Infof("found multicluster public service names %v", svcNames)

	headlessMirrorReq, err := labels.NewRequirement(labelKeyHeadlessMirror, selection.In, svcNames)
	if err != nil {
		return out, fmt.Errorf("could not construct headless mirror requirement: %w", err)
	}
	selector := labels.NewSelector()
	selector = selector.Add(*headlessMirrorReq)
	logrus.Debugf("listing services using label selector %s", selector.String())
	opts := metav1.ListOptions{
		LabelSelector: selector.String(),
	}
	serviceList, err := cli.cliSet.CoreV1().Services(cli.namespaceName).List(ctx, opts)
	if err != nil {
		return out, err
	}
	if serviceList == nil {
		return out, fmt.Errorf("endpoint list pointer cannot be nil")
	}
	return serviceList.Items, nil
}

// watcherList also implements the watcher.Interface.
type watcherList []watch.Interface

var _ watch.Interface = watcherList{}

// Stop stops all watchers contained in the list.
func (l watcherList) Stop() {
	for _, watcher := range l {
		watcher.Stop()
	}
}

// ResultChan merges all result channels of the watchers contained in the list.
func (l watcherList) ResultChan() <-chan watch.Event {
	eventCh := make(chan watch.Event)
	var wg sync.WaitGroup
	for _, watcher := range l {
		wg.Add(1)
		go func(ch <-chan watch.Event) {
			for event := range ch {
				eventCh <- event
			}
			wg.Done()
		}(watcher.ResultChan())
	}
	go func() {
		wg.Wait()
		close(eventCh)
	}()
	return eventCh
}
