package cmd

import (
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// withActivationProxy sets up the magnolia activation proxy handler. The target
// should be the same as it would be registered in an author instance as a
// subscriber (omit '.magnolia/activation').
func (s *server) withActivationProxy(rawTarget string) error {
	target, err := url.Parse(rawTarget)
	if err != nil {
		return err
	}

	errw := logrus.StandardLogger().WriterLevel(logrus.ErrorLevel)
	infow := logrus.StandardLogger().WriterLevel(logrus.InfoLevel)

	proxy := httputil.NewSingleHostReverseProxy(target)
	proxy.ErrorLog = log.New(errw, "proxy: ", 0)

	logh := handlers.LoggingHandler(infow, proxy)

	s.r.HandleFunc("/.magnolia/activation", s.handleActivation(logh))
	s.r.HandleFunc("/activation/lock/{name}", s.handleLock()).Methods(http.MethodPost)
	s.r.HandleFunc("/activation/lock/{name}", s.handleUnlock()).Methods(http.MethodDelete)
	return nil
}

const (
	hdrAttributeStatus  = "sa_attribute_status"
	hdrAttributeMessage = "sa_attribute_message"

	// https://git.magnolia-cms.com/projects/MODULES/repos/publishing/browse/magnolia-publishing-core/src/main/java/info/magnolia/publishing/Status.java
	activationStatusSuccess = "sa_success"
	activationStatusFailure = "sa_failed"
)

func (s *server) handleActivation(proxy http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !s.readinessCheck.IsSuccess() {
			http.Error(w, "health check failed, can't activate if app is unhealthy", http.StatusServiceUnavailable)
			return
		}
		if s.al.IsLocked() {
			msg := "activation locked, try again later"
			w.Header().Add(hdrAttributeStatus, activationStatusFailure)
			w.Header().Add(hdrAttributeMessage, msg)
			http.Error(w, msg, http.StatusNoContent)
			return
		}
		proxy.ServeHTTP(w, r)
	}
}

func (s *server) handleLock() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		name, ok := mux.Vars(r)["name"]
		if !ok {
			http.Error(rw, "missing activation lock name", http.StatusBadRequest)
			return
		}
		start := time.Now()
		d := s.al.Lock(name, func() {
			logrus.Infof("activation lock %s auto-unlocked after %s", name, time.Since(start))
		})
		logrus.Infof("activation lock %s locked, unlocking after %s", name, d)
	}
}

func (s *server) handleUnlock() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		name, ok := mux.Vars(r)["name"]
		if !ok {
			http.Error(rw, "missing activation lock name", http.StatusBadRequest)
			return
		}
		if s.al.Unlock(name) {
			logrus.Infof("activation lock %s unlocked", name)
		}
		// We don't care in this case.
	}
}
