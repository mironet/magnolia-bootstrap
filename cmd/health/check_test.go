package health

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func Test_healthCheck_IsSuccess(t *testing.T) {
	successSingle, _ := NewCheck(
		"successful check",
		func() error {
			return nil
		},
		WithInterval(time.Microsecond),
		WithMaxInterval(2*time.Microsecond),
	)
	successSingle2, _ := NewCheck(
		"successful check",
		func() error {
			return nil
		},
		WithInterval(time.Microsecond),
		WithMaxInterval(2*time.Microsecond),
	)
	failSingle, _ := NewCheck(
		"failed check",
		func() error {
			return fmt.Errorf("this always fails, sorry")
		},
	)
	childFail, _ := NewCheck(
		"failed because child failed",
		func() error {
			// We ourselves do not fail.
			return nil
		},
		WithChild(failSingle),
	)
	successChain, _ := NewCheck(
		"chained success",
		func() error {
			return nil
		},
		WithChild(successSingle2),
		WithInterval(time.Microsecond),
		WithMaxInterval(2*time.Microsecond),
	)

	tests := []struct {
		name    string
		h       *Check
		waitFor time.Duration
		want    bool
	}{
		{
			name: "single successful test",
			h:    successSingle,
			want: true,
		},
		{
			name: "single successful test",
			h:    failSingle,
			want: false,
		},
		{
			name: "chained fail, child fails",
			h:    childFail,
			want: false,
		},
		{
			name: "chained success",
			h:    successChain,
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			start := time.Now()
			h := tt.h
			ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
			defer cancel()
			h.Run(ctx)
			// Give goroutines enough time for the exponential backoff.
			if tt.want == true {
				h.Wait(context.Background()) // nolint: errcheck
			}
			got := h.IsSuccess()
			if got != tt.want {
				t.Errorf("%s: %s: healthCheck.IsSuccess() got = %v, want %v", time.Since(start), tt.name, got, tt.want)
			}
			t.Logf("time taken: %s", time.Since(start))
		})
	}
}

func TestStatus_MarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		s       Status
		want    []byte
		wantErr bool
	}{
		{
			name:    "zero value",
			want:    []byte(`"fail"`),
			wantErr: false,
		},
		{
			name:    "success",
			s:       StatusPass,
			want:    []byte(`"pass"`),
			wantErr: false,
		},
		{
			name:    "explicit fail",
			s:       StatusFail,
			want:    []byte(`"fail"`),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.MarshalJSON()
			if (err != nil) != tt.wantErr {
				t.Errorf("Status.MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Status.MarshalJSON() = %s, want %s", got, tt.want)
			}
		})
	}
}
