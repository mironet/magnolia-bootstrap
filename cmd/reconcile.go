package cmd

import (
	"fmt"
	"mgnlboot/cmd/magnolia"
	"sort"

	"github.com/magiconair/properties"
	"github.com/sirupsen/logrus"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type nodeOperation int

const (
	keepNode nodeOperation = iota
	updateNode
	deleteNode
)

func newReceiverNode(nodeName, address string) *magnolia.Node {
	return &magnolia.Node{
		Name: nodeName,
		Type: magnolia.ContentNodeType,
		Properties: []*magnolia.Property{
			{
				Name:   magnolia.UrlPropertyName,
				Type:   magnolia.StringPropertyType,
				Values: []string{address},
			},
			{
				Name:   magnolia.EnabledPropertyName,
				Type:   magnolia.StringPropertyType,
				Values: []string{"true"},
			},
		},
	}
}

type byHostname []corev1.EndpointAddress

func (a byHostname) Len() int {
	return len(a)
}

func (a byHostname) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a byHostname) Less(i, j int) bool {
	return a[i].Hostname < a[j].Hostname
}

// reconcileReceiversV6_3 reconciles the receivers starting from Magnolia 6.3.
func (s *server) reconcileReceiversV6_3() error {
	list := func() ([]publishingReceiver, error) {
		// Find the existing microprofile config map.
		mircoConfMap, err := s.k8sCli.microprofileConfigMap(s.ctx)
		if err != nil {
			return nil, fmt.Errorf("could not get microprofile config map: %w", err)
		}

		// Extract the microprofile config from the config map.
		microProps, ok := mircoConfMap.Data[dataKeyAuthorMicroprofConfig]
		if !ok {
			return nil, fmt.Errorf("could not find data key %s in config map %s", dataKeyAuthorMicroprofConfig, mircoConfMap.Name)
		}
		microConf := &microprofileConf{}
		if err := microConf.UnmarshalText([]byte(microProps)); err != nil {
			return nil, fmt.Errorf("could not unmarshal microprofile config: %w", err)
		}
		return microConf.receivers, nil
	}

	reconcile := func(receiver publishingReceiver, action receiverReconcileAction) error {
		// Get the list of receivers we currently have ...
		have, err := list()
		if err != nil {
			return fmt.Errorf("could not list receivers: %w", err)
		}

		// ... and update it according to the action.
		switch action {
		case addReceiver:
			have = append(have, receiver)
		case updateReceiver:
			for i, haveReceiver := range have {
				if haveReceiver.name == receiver.name {
					have[i] = receiver
					break
				}
			}
		case deleteReceiver:
			for i, haveReceiver := range have {
				if haveReceiver.name == receiver.name {
					have = append(have[:i], have[i+1:]...)
					break
				}
			}
		default:
			return fmt.Errorf("encountered unknown action with action number %d", action)
		}

		// Find the existing microprofile config map.
		mircoConfMap, err := s.k8sCli.microprofileConfigMap(s.ctx)
		if err != nil {
			return fmt.Errorf("could not get existing microprofile config map: %w", err)
		}

		// Extract the existing microprofile config from the config map.
		existingMicroProps, ok := mircoConfMap.Data[dataKeyAuthorMicroprofConfig]
		if !ok {
			return fmt.Errorf("could not find data key %s in config map %s", dataKeyAuthorMicroprofConfig, mircoConfMap.Name)
		}

		// Update the data in the microprofile config map using the existing
		// properties and the updated receivers list.
		properties, err := properties.LoadString(string(existingMicroProps))
		if err != nil {
			return fmt.Errorf("could not load existing microprofile properties from string: %w", err)
		}
		microConf := &microprofileConf{
			receivers:  have,
			properties: properties,
		}
		microProps, err := microConf.MarshalText()
		if err != nil {
			return fmt.Errorf("could not marshal microprofile config: %w", err)
		}
		mircoConfMap.Data[dataKeyAuthorMicroprofConfig] = string(microProps)

		// Update the existing microprofile config map.
		if _, err := s.k8sCli.cliSet.CoreV1().ConfigMaps(s.k8sCli.namespaceName).Update(s.ctx, &mircoConfMap, metav1.UpdateOptions{}); err != nil {
			return fmt.Errorf("could not update microprofile config map %s: %w", mircoConfMap.Name, err)
		}
		return nil
	}

	return s.reconcileReceivers(list, reconcile)
}

// reconcileReceiversV6_2 reconciles the receivers in Magnolia 6.2.x and older.
func (s *server) reconcileReceiversV6_2() error {
	list := func() ([]publishingReceiver, error) {
		receiverNode := new(magnolia.Node)
		err := s.mgnlCli.Get(s.ctx, magnolia.ReceiversPath, 1, receiverNode)
		if err != nil {
			return nil, fmt.Errorf("could not get receiver node: %w", err)
		}

		receivers := make([]publishingReceiver, 0)
		for _, node := range receiverNode.Nodes {
			receiver := publishingReceiver{
				name: node.Name,
			}
			for _, prop := range node.Properties {
				if prop.Type != magnolia.StringPropertyType || len(prop.Values) == 0 {
					continue
				}
				if prop.Name == magnolia.UrlPropertyName {
					receiver.url = prop.Values[0]
				}
				if prop.Name == magnolia.EnabledPropertyName {
					receiver.enabled = prop.Values[0] == "true"
				}
			}
			if receiver.enabled {
				receivers = append(receivers, receiver)
			}
		}
		return receivers, nil
	}

	reconcile := func(receiver publishingReceiver, action receiverReconcileAction) error {
		if !receiver.enabled {
			return fmt.Errorf("cannot handle disabled receiver %s: not implemented", receiver.name)
		}

		node := newReceiverNode(receiver.name, receiver.url)
		switch action {
		case addReceiver:
			err := s.mgnlCli.AddReceiversNode(s.ctx, node)
			if err != nil {
				return fmt.Errorf("could not add node %s", node.Name)
			}
		case updateReceiver:
			err := s.mgnlCli.UpdateReceiversNode(s.ctx, node)
			if err != nil {
				return fmt.Errorf("could not update node %s", node.Name)
			}
		case deleteReceiver:
			err := s.mgnlCli.DeleteReceiversNode(s.ctx, node)
			if err != nil {
				return fmt.Errorf("could not delete node %s", node.Name)
			}

		default:
			return fmt.Errorf("encountered unknown action with action number %d", action)
		}
		return nil
	}
	return s.reconcileReceivers(list, reconcile)
}

type publishingReceiver struct {
	name    string
	url     string
	enabled bool
}

func (r publishingReceiver) Equals(in publishingReceiver) (bool, string) {
	if r.name != in.name {
		return false, fmt.Sprintf("unequal names %s != %s", r.name, in.name)
	}
	if r.url != in.url {
		return false, fmt.Sprintf("unequal urls %s != %s", r.url, in.url)
	}
	if r.enabled != in.enabled {
		return false, fmt.Sprintf("unequal enabled flag %t != %t", r.enabled, in.enabled)
	}
	return true, ""
}

type receiverReconcileAction int

const (
	addReceiver receiverReconcileAction = iota
	updateReceiver
	deleteReceiver
)

type reconcileReceiverFunc func(receiver publishingReceiver, action receiverReconcileAction) error

type listReceiversFunc func() ([]publishingReceiver, error)

func (s *server) reconcileReceivers(list listReceiversFunc, reconcile reconcileReceiverFunc) error {
	// Find out what receiver nodes we got.
	got, err := list()
	if err != nil {
		return fmt.Errorf("could not list receivers we got: %w", err)
	}

	// Find public endpoints.
	want := make([]publishingReceiver, 0)
	publicEndpoints, err := s.k8sCli.publicEndpoints(s.ctx)
	if err != nil {
		return fmt.Errorf("could not get public endpoints list: %w", err)
	}

	// For each subset in the endpoints element.
	for _, subset := range publicEndpoints.Subsets {
		// Find its cloud-sidecar port.
		cloudSidecarPort := int32(-1)
		for _, port := range subset.Ports {
			if port.Name != "cloud-sidecar" {
				continue
			}
			cloudSidecarPort = port.Port
		}
		if cloudSidecarPort == -1 {
			return fmt.Errorf("could not find cloud-sidecar port in subset %v", subset)
		}

		// And add a receiver for each of its addresses (also the not ready
		// addresses). Note: Not ready addresses are added to block the magnolia
		// author from publishing as long as not all the receivers are ready.
		allAdresses := append(subset.Addresses, subset.NotReadyAddresses...)
		sort.Sort(byHostname(allAdresses))
		for i, address := range allAdresses {
			receiverName := publicEndpoints.Name
			if i > 0 {
				receiverName = fmt.Sprintf("%s-%d", receiverName, i)
			}
			want = append(want, publishingReceiver{
				name:    receiverName,
				url:     fmt.Sprintf("http://%s.%s:%d/", address.Hostname, publicEndpoints.Name, cloudSidecarPort),
				enabled: true,
			})
		}
	}

	// Also find possible remote public services (for multicluster
	// scenario).
	remoteServices, err := s.k8sCli.remoteServiceList(s.ctx)
	if err != nil {
		return fmt.Errorf("could not get remote service list: %w", err)
	}

	// For each remote service, find its cloud-sidecar port and add a receiver
	// for it.
	for _, svc := range remoteServices {
		cloudSidecarPort := int32(-1)
		for _, port := range svc.Spec.Ports {
			if port.Name != "cloud-sidecar" {
				continue
			}
			cloudSidecarPort = port.Port
		}
		if cloudSidecarPort == -1 {
			return fmt.Errorf("could not find cloud-sidecar port in service %s", svc.Name)
		}
		want = append(want, publishingReceiver{
			name:    svc.Name,
			url:     fmt.Sprintf("http://%s:%d/", svc.Name, cloudSidecarPort),
			enabled: true,
		})
	}

	// Loop through all the receivers we have got to find out whether to delete,
	// keep or update them.
	for _, gRec := range got {
		// Initially we assume that we must delete the receiver that we have.
		nodeOp := deleteNode
		for _, wRec := range want {
			if gRec.name != wRec.name {
				continue
			}
			// If we want the receiver that we have.

			// Compare the receiver properties.
			isEqual, _ := gRec.Equals(wRec)
			switch isEqual {
			case true:
				// If the receiver we got is equal to the receiver we want, we
				// can leave it as it is.
				nodeOp = keepNode
			case false:
				// If the receiver we got is not equal to the receiver we want,
				// we need to update the it.
				gRec = wRec
				nodeOp = updateNode
			}
		}

		switch nodeOp {
		case deleteNode:
			if s.deactivateReceiverDeletion {
				logrus.Infof("not deleting receiver %s", gRec.name)
			} else {
				err := reconcile(gRec, deleteReceiver)
				if err != nil {
					return err
				}
				logrus.Infof("deleted receiver %s", gRec.name)
			}
		case updateNode:
			err := reconcile(gRec, updateReceiver)
			if err != nil {
				return err
			}
			logrus.Infof("updated receiver %s", gRec.name)
		case keepNode:
			// Don't do anything.
		}
	}

	// If the receiver creation is deactivated, we can stop here.
	if s.deactivateReceiverCreation {
		return nil
	}

	// Loop through all the receivers we want, to find out whether we have to
	// add any receivers.
	for _, wRec := range want {
		exists := false
		for _, gRec := range got {
			areRecsEqual, _ := gRec.Equals(wRec)
			if gRec.name == wRec.name && areRecsEqual {
				exists = true
			}
		}
		if !exists {
			err := reconcile(wRec, addReceiver)
			if err != nil {
				return err
			}
			logrus.Infof("added receiver %s", wRec.name)
		}
	}

	return nil
}
