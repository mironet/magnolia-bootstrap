package magnolia

import (
	"io/fs"
	"testing"
	"testing/fstest"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestGetVersion(t *testing.T) {
	var v6238 = fstest.MapFS{
		"webapps/magnolia/WEB-INF/lib/magnolia-core-6.2.38.jar": {
			Data: []byte("I'm Magnolia Version 6.2.38"),
		},
	}
	var noMagnolia = fstest.MapFS{
		"magnolia-dam-app-compatibility-3.0.25.jar": {},
		"magnolia-dam-app-jcr-3.0.25.jar":           {},
	}
	var snapshotVersion = fstest.MapFS{
		"webapps/magnolia/WEB-INF/lib/magnolia-core-6.2-SNAPSHOT.jar": {
			Data: []byte("I'm Magnolia Version 6.2-SNAPSHOT"),
		},
	}
	type args struct {
		fsys fs.FS
	}
	tests := []struct {
		name    string
		args    args
		want    *Version
		wantErr bool
	}{
		{
			name:    "success",
			args:    args{v6238},
			want:    &Version{Maj: 6, Min: 2, Patch: "38"},
			wantErr: false,
		},
		{
			name:    "no magnolia version present",
			args:    args{noMagnolia},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "snapshot version",
			args:    args{snapshotVersion},
			want:    &Version{Maj: 6, Min: 2, Patch: "SNAPSHOT"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetVersion(tt.args.fsys)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetVersion() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if diff := cmp.Diff(got, tt.want, cmpopts.IgnoreUnexported(Version{})); diff != "" {
				t.Error(diff)
			}
		})
	}
}

func TestVersion_Less(t *testing.T) {
	type fields struct {
		Maj   int
		Min   int
		Patch string
	}
	type args struct {
		other Version
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "less",
			fields: fields{Maj: 6, Min: 2, Patch: "33"},
			args:   args{MustFromString("6.2.38")},
			want:   true,
		},
		{
			name:   "more",
			fields: fields{Maj: 6, Min: 2, Patch: "38"},
			args:   args{MustFromString("6.2.33")},
			want:   false,
		},
		{
			name:   "equal",
			fields: fields{Maj: 6, Min: 2, Patch: "33"},
			args:   args{MustFromString("6.2.33")},
			want:   false,
		},
		{
			name:   "patch as string",
			fields: fields{Maj: 6, Min: 2, Patch: "33-debug2"},
			args:   args{MustFromString("6.2.33-debug")},
			want:   false,
		},
		{
			name:   "only other's patch as string",
			fields: fields{Maj: 6, Min: 2, Patch: "33"},
			args:   args{MustFromString("6.2.33-debug")},
			want:   true, // TODO: Is that really what we want? (corner case?)
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := Version{
				Maj:   tt.fields.Maj,
				Min:   tt.fields.Min,
				Patch: tt.fields.Patch,
			}
			if got := v.Less(tt.args.other); got != tt.want {
				t.Errorf("Version.Less() = %v, want %v", got, tt.want)
			}
		})
	}
}
