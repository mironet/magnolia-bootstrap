package magnolia

import (
	"context"
	"errors"
	"fmt"
	"mgnlboot/cmd/api"
	"net/http"
	"net/url"
	"path"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/sync/semaphore"
)

const (
	ReceiversPath              = "config/modules/publishing-core/config/receivers"
	SuperuserPwPath            = "users/system"
	UrlPropertyName            = "url"
	EnabledPropertyName        = "enabled"
	StringPropertyType         = "String"
	PasswordPropertyName       = "pswd"
	DefaultBaseURLPropertyName = "defaultBaseUrl"
)

const (
	baseURLTpl      = "%s/.rest/%s/" // First %s is the scheme://host part.
	defaultEndpoint = "nodes/v1"     // We use the node endpoint if none given.

	serverConfigPath                  = "config/server"
	activationPath                    = "config/server/activation"
	activationManagerAdapterClassName = "info.magnolia.publishing.manager.ActivationManagerAdapter"

	publicKeyPropertyName = "publicKey"
	classPropertyName     = "class"
)

// Client is a magnolia REST API client.
type Client struct {
	*api.Client

	superuserPWSem *semaphore.Weighted
	Endpoint       string // Endpoint part in the Magnolia REST API path.
}

// Clone this client with all its settings.
func (c *Client) Clone(opts ...api.Opt) (*Client, error) {
	cli, err := c.Client.Clone(opts...)
	if err != nil {
		return nil, err
	}
	return &Client{
		Client:         cli,
		Endpoint:       c.Endpoint,
		superuserPWSem: semaphore.NewWeighted(1),
	}, nil
}

// BaseURL returns the API endpoint url after parsing u.
func BaseURL(address, endpoint string) (*url.URL, error) {
	if endpoint == "" {
		endpoint = defaultEndpoint
	}
	base, err := url.Parse(fmt.Sprintf(baseURLTpl, address, endpoint))
	if err != nil {
		return nil, fmt.Errorf("cannot parse %s as BaseURL, endpoint = %s: %w", address, endpoint, err)
	}

	base.Path = path.Clean(base.Path) + "/"
	return base, nil
}

// Opt is a Magonlia client option.
type Opt func(*Client) error

// OptFunc returns a an Opt if f is an api.Opt.
func OptFunc(f api.Opt) Opt {
	return func(c *Client) error {
		if err := f(c.Client); err != nil {
			return err
		}
		return nil
	}
}

// OptFuncs transforms api.Opts to Opts. See OptFunc.
func OptFuncs(opts []api.Opt) []Opt {
	var out = make([]Opt, 0)
	for _, f := range opts {
		out = append(out, OptFunc(f))
	}
	return out
}

// WithEndpoint sets the REST API endpoint part in the path.
func WithEndpoint(endpoint string) Opt {
	return func(c *Client) error {
		c.Endpoint = endpoint
		return nil
	}
}

// WithBaseURL sets the BaseURL.
func WithBaseURL(address string) Opt {
	return func(c *Client) error {
		base, err := BaseURL(address, c.Endpoint)
		if err != nil {
			return err
		}
		c.BaseURL = base
		return nil
	}
}

// NewClient returns a new Magnolia REST API client.
func NewClient(opts ...Opt) (*Client, error) {
	c, err := api.New()
	if err != nil {
		return nil, err
	}
	cli := &Client{
		Client:         c,
		superuserPWSem: semaphore.NewWeighted(1),
	}
	for _, opt := range opts {
		if err := opt(cli); err != nil {
			return nil, err
		}
	}
	return cli, nil
}

type Config struct {
	Username  string
	Password  string
	Insecure  bool
	Mgnl      string
	Endpoint  string
	Verbosity int
	Token     string

	UseJWTTokenIssuer bool
	TokenLife         time.Duration
	EarlyRefreshBias  time.Duration
	PrivateKey        []byte
}

func NewClientFromConfig(conf Config) (*Client, error) {
	var authOpt api.Opt
	switch {
	case conf.UseJWTTokenIssuer:
		logrus.Info("new magnolia client uses jwt token issuer for authorization")
		tokenIssuer, err := api.NewJWTTokenIssuer(
			conf.TokenLife, conf.EarlyRefreshBias, conf.PrivateKey,
			"magnolia-bootstrapper", "magnolia-bootstrapper", "magnolia",
		)
		if err != nil {
			return nil, fmt.Errorf("could not get new jwt token issuer: %w", err)
		}
		authOpt = api.WithJWTTokenIssuer(tokenIssuer)
	case conf.Username == "" && conf.Password == "" && conf.Token != "":
		logrus.Info("new magnolia client uses bearer token for authorization")
		authOpt = api.WithAuthorization(conf.Token)
	case conf.Username != "" && conf.Password != "" && conf.Token == "":
		logrus.Info("new magnolia client uses basic auth for authorization")
		authOpt = api.WithBasicAuth(conf.Username, conf.Password)
	default:
		return nil, fmt.Errorf(
			"either username+password or token must be provided, but got username provided=%t, password provided=%t, token provided=%t",
			conf.Username != "", conf.Password != "", conf.Token != "",
		)
	}

	cli, err := NewClient(
		WithEndpoint(conf.Endpoint),
		WithBaseURL(conf.Mgnl),
		OptFunc(authOpt),
		OptFunc(api.WithVerbosity(conf.Verbosity)),
		OptFunc(api.WithInsecure(conf.Insecure)),
	)
	if err != nil {
		return nil, fmt.Errorf("could not initialize magnolia client: %w", err)
	}
	return cli, nil
}

// Get a JCR node with the given depth of recursion, default is 1. The result
// will be filled into *node. If anything different happens, an error is
// returned.
func (c *Client) Get(ctx context.Context, location string, depth int, node *Node) error {
	location = fmt.Sprintf("%s?depth=%d", location, depth)
	req, err := c.NewRequest(ctx, http.MethodGet, location, nil)
	if err != nil {
		return fmt.Errorf("could not create new HTTP request: %w", err)
	}

	_, err = c.Do(ctx, req, node)
	if err != nil {
		return fmt.Errorf("could not execute HTTP request: %w", err)
	}

	return nil
}

func (c *Client) AddReceiversNode(ctx context.Context, node *Node) error {
	req, err := c.NewRequest(ctx, http.MethodPut, ReceiversPath, node)
	if err != nil {
		return fmt.Errorf("could not create new HTTP request: %w", err)
	}

	_, err = c.Do(ctx, req, nil)
	if err != nil {
		return fmt.Errorf("could not execute HTTP request: %w", err)
	}
	return nil
}

func (c *Client) UpdateReceiversNode(ctx context.Context, node *Node) error {
	req, err := c.NewRequest(ctx, http.MethodPost, filepath.Join(ReceiversPath, node.Name), node)
	if err != nil {
		return fmt.Errorf("could not create new HTTP request: %w", err)
	}

	_, err = c.Do(ctx, req, nil)
	if err != nil {
		return fmt.Errorf("could not execute HTTP request: %w", err)
	}
	return nil
}

func (c *Client) DeleteReceiversNode(ctx context.Context, node *Node) error {
	req, err := c.NewRequest(ctx, http.MethodDelete, filepath.Join(ReceiversPath, node.Name), nil)
	if err != nil {
		return fmt.Errorf("could not create new HTTP request: %w", err)
	}

	_, err = c.Do(ctx, req, nil)
	if err != nil {
		return fmt.Errorf("could not execute HTTP request: %w", err)
	}
	return nil
}

// The error returned by IsSuperuserPW() when the current password is not
// bcrypted.
var ErrPasswordNotBcrypted = errors.New("current password is not bcrypted")

func (c *Client) IsSuperuserPW(ctx context.Context, superuserName, pw string) (bool, error) {
	// Get the superuser node.
	superuserPath := fmt.Sprintf("%s/%s", SuperuserPwPath, superuserName)
	n := &Node{}
	if err := c.Get(ctx, superuserPath, 0, n); err != nil {
		return false, fmt.Errorf("could not get superuser node: %w", err)
	}

	// Get the password hash from the superuser node.
	var hash string
	for _, prop := range n.Properties {
		if prop.Name == PasswordPropertyName && len(prop.Values) == 1 {
			hash = prop.Values[0]
		}
	}
	if hash == "" {
		return false, fmt.Errorf("did not find existing password hash")
	}

	// Check if the given password checks out with the superuser password hash.
	if err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pw)); err != nil {
		if !errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return false, ErrPasswordNotBcrypted
		}
		return false, nil
	}
	return true, nil
}

var ErrSuperuserSemaphorTaken = errors.New("superuser semaphore is taken: try again later")

func (c *Client) ReenableSuperuser(ctx context.Context, superuserName string) error {
	if c.superuserPWSem != nil && !c.superuserPWSem.TryAcquire(1) {
		logrus.Error(ErrSuperuserSemaphorTaken)
		return ErrSuperuserSemaphorTaken
	}
	defer c.superuserPWSem.Release(1)
	logrus.Infof("reenabeling superuser %s", superuserName)
	n := &Node{
		Properties: []*Property{
			{
				Name:   EnabledPropertyName,
				Type:   StringPropertyType,
				Values: []string{"true"},
			},
		},
	}
	superuserPath := fmt.Sprintf("%s/%s", SuperuserPwPath, superuserName)
	if err := c.putOrPostNode(ctx, superuserPath, n); err != nil {
		return fmt.Errorf("could not put or post node at path %s: %w", superuserPath, err)
	}
	logrus.Infof("successfully reenabled superuser %s", superuserName)
	return nil
}

func (c *Client) SetSuperuserPw(ctx context.Context, superuserName, pw string) error {
	if c.superuserPWSem != nil && !c.superuserPWSem.TryAcquire(1) {
		logrus.Error(ErrSuperuserSemaphorTaken)
		return ErrSuperuserSemaphorTaken
	}
	defer c.superuserPWSem.Release(1)
	logrus.Infof("setting superuser password for %s", superuserName)
	hash, err := bcrypt.GenerateFromPassword([]byte(pw), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("could not crypt password: %w", err)
	}
	n := &Node{
		Properties: []*Property{
			{
				Name:   PasswordPropertyName,
				Type:   StringPropertyType,
				Values: []string{string(hash)},
			},
		},
	}
	superuserPath := fmt.Sprintf("%s/%s", SuperuserPwPath, superuserName)
	if err := c.putOrPostNode(ctx, superuserPath, n); err != nil {
		return fmt.Errorf("could not put or post node at path %s: %w", superuserPath, err)
	}
	logrus.Infof("successfully set superuser password for %s", superuserName)
	return nil
}

func (c *Client) SetDefaultBaseURL(ctx context.Context, defaultBaseURL string) error {
	n := &Node{
		Properties: []*Property{
			{
				Name:   DefaultBaseURLPropertyName,
				Type:   StringPropertyType,
				Values: []string{defaultBaseURL},
			},
		},
	}
	if err := c.putOrPostNode(ctx, serverConfigPath, n); err != nil {
		return fmt.Errorf("could not put or post node at path %s: %w", serverConfigPath, err)
	}
	return nil
}

func (c *Client) GetPublicKey(ctx context.Context) (string, error) {
	activationNode := &Node{}
	if err := c.Get(ctx, activationPath, 1, activationNode); err != nil {
		return "", fmt.Errorf("could not get activation node: %w", err)
	}
	for _, property := range activationNode.Properties {
		if property.Name == publicKeyPropertyName && len(property.Values) == 1 {
			return property.Values[0], nil
		}
	}
	return "", fmt.Errorf("activation node has no public key property")
}

func (c *Client) SetPublicKey(ctx context.Context, publicKey string) error {
	n := &Node{
		Properties: []*Property{
			{
				Name:   classPropertyName,
				Type:   StringPropertyType,
				Values: []string{activationManagerAdapterClassName},
			},
			{
				Name:   publicKeyPropertyName,
				Type:   StringPropertyType,
				Values: []string{publicKey},
			},
		},
	}
	if err := c.putOrPostNode(ctx, activationPath, n); err != nil {
		return fmt.Errorf("could not put or post node at path %s: %w", activationPath, err)
	}
	return nil
}

func (c *Client) putOrPostNode(ctx context.Context, path string, n *Node) error {
	// Check whether we need to post or put the node at the given path.
	method, err := c.methodForNodeAtPath(ctx, path)
	if err != nil {
		return fmt.Errorf("could not get method for node: %w", err)
	}

	// Create or update node.
	req, err := c.NewRequest(ctx, method, path, n)
	if err != nil {
		return fmt.Errorf("could not create new HTTP request: %w", err)
	}
	_, err = c.Do(ctx, req, nil)
	if err != nil {
		return fmt.Errorf("could not execute HTTP %s request on %s: %w", req.Method, req.URL, err)
	}
	return nil
}

func (c *Client) methodForNodeAtPath(ctx context.Context, path string) (string, error) {
	// Check whether superuser node already exists.
	req, err := c.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return "", fmt.Errorf("could not get new request: %w", err)
	}
	var node Node
	method := http.MethodPost
	if _, err := c.Do(ctx, req, &node); err != nil {
		var e *api.ErrorResponse
		if !errors.As(err, &e) {
			return "", fmt.Errorf("could not do request: %w", err)
		}
		if e.Response.StatusCode == http.StatusNotFound {
			method = http.MethodPut
		}
	}
	return method, nil
}
