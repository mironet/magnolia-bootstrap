package magnolia

import (
	"errors"
	"fmt"
	"io/fs"
	"regexp"
	"strconv"
	"strings"
)

var (
	reMagnoliaJar = regexp.MustCompile(`^magnolia-core-([0-9]+.[0-9]+[.-][0-9a-zA-Z]+[.-]*[0-9a-zA-Z]*).jar$`)
	reMagnoliaVer = regexp.MustCompile(`^([0-9]+).([0-9]+)([.-][0-9a-zA-Z]+[.-]*[0-9a-zA-Z]*)$`)
	errNoMagnolia = fmt.Errorf("no magnolia present")
)

type Version struct {
	Maj         int
	Min         int
	Patch       string
	patchPrefix string
}

func (v Version) String() string {
	return fmt.Sprintf("%d.%d%s%s", v.Maj, v.Min, v.patchPrefix, v.Patch)
}

// FromString creates a version object from a string.
func FromString(in string) (*Version, error) {
	ps := reMagnoliaVer.FindStringSubmatch(in)
	if ps == nil {
		return nil, fmt.Errorf("string doesn't appear to be a version (maj.min.patch): no match: %s", in)
	}
	if len(ps) != 4 {
		return nil, fmt.Errorf("string doesn't appear to be a version (maj.min.patch): %s", in)
	}
	ps = ps[1:] // Remove the first element which is the whole string.
	maj, err := strconv.Atoi(ps[0])
	if err != nil {
		return nil, fmt.Errorf("could not parse major version (%s) as number: %w", ps[0], err)
	}
	min, err := strconv.Atoi(ps[1])
	if err != nil {
		return nil, fmt.Errorf("could not parse minor version (%s) as number: %w", ps[1], err)
	}
	patch := ps[2]
	var prefix string
	// Remove the prefix if present.
	if strings.HasPrefix(patch, "-") || strings.HasPrefix(patch, ".") {
		prefix = patch[:1]
		patch = patch[1:]
	}
	return &Version{
		Maj:         maj,
		Min:         min,
		Patch:       patch,
		patchPrefix: prefix,
	}, nil
}

// Same as FromString but panics in case of an error.
func MustFromString(in string) Version {
	v, err := FromString(in)
	if err != nil {
		panic(err)
	}
	return *v
}

// Less returns true if "v" is an earlier version than "other".
func (v Version) Less(other Version) bool {
	if v.Maj < other.Maj {
		return true
	}
	if v.Maj > other.Maj {
		return false
	}
	// v.Maj is equal to other.Maj
	if v.Min < other.Min {
		return true
	}
	if v.Min > other.Min {
		return false
	}
	// v.Maj, v.Min are equal to other.Maj, other.Min. Let's try to parse both
	// "patch" fields as numbers.
	tp, err := strconv.Atoi(v.Patch)
	if err != nil {
		return v.Patch < other.Patch
	}
	op, err := strconv.Atoi(other.Patch)
	if err != nil {
		return v.Patch < other.Patch
	}
	return tp < op
}

// GetVersion reads the version from the jars in the filesystem passed. It will
// walk the filesystem and try to find the `magnolia-core` .jar file. This is
// very ugly but apparently the only way to (more or less) reliably get the
// currently installed Magnolia version.
func GetVersion(fsys fs.FS) (*Version, error) {
	var ver *Version
	var oerr error
	fserr := fs.WalkDir(fsys, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			return nil // We are not interested in directories.
		}
		ps := reMagnoliaJar.FindStringSubmatch(d.Name())
		if ps == nil {
			return nil // We are not interested in files that don't match.
		}
		if len(ps) != 2 {
			return fmt.Errorf("string doesn't appear to be a version (maj.min.patch): %s", d.Name())
		}
		str := ps[1] // This should be the version string.
		ver, err = FromString(str)
		if err != nil {
			oerr = fmt.Errorf("could not parse %s into version string: %w", str, err)
			return fs.SkipAll
		}
		return fs.SkipAll
	})
	if fserr != nil {
		if errors.Is(fserr, fs.SkipAll) {
			return ver, oerr
		}
		return nil, fmt.Errorf("error walking the file system: %w", fserr)
	}
	if ver == nil {
		return nil, errNoMagnolia
	}
	return ver, nil
}
