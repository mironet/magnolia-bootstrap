package cmd

import (
	"context"
	"errors"
	"fmt"
	"mgnlboot/cmd/api"
	"mgnlboot/cmd/magnolia"
	"net/http"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
)

// Executor executes stuff.
type Executor struct {
	cli    *magnolia.Client
	list   *InstructionList
	dryrun bool
}

// NewExecutor creates a new executor.
func NewExecutor(cli *magnolia.Client, list *InstructionList, dryrun bool) *Executor {
	e := &Executor{
		cli:    cli,
		list:   list,
		dryrun: dryrun,
	}
	return e
}

// Run the executor.
func (e *Executor) Run(ctx context.Context) error {
	if e.list == nil {
		return nil // In case the instruction list is empty, we just quit.
	}
	errcList := make([]<-chan error, 0)

	errc := executeInstructions(ctx, e.Do, e.list)
	errcList = append(errcList, errc)

	return waitForPipeline(errcList...)
}

func (e *Executor) HasEmptyInstructionList() bool {
	return e.list == nil
}

type executorFunc func(context.Context, *Instruction) error

func executeInstructions(ctx context.Context, execute executorFunc, il *InstructionList) <-chan error {
	max := 4
	sem := make(chan struct{}, max)
	errc := make(chan error, max)

	go func() {
		defer close(errc)

		for _, ins := range il.Playbook {
			// Execute the same instruction for multiple endpoints concurrently,
			// but stay within max goroutines.
			var wg sync.WaitGroup

			for _, ep := range ins.Endpoints {
				select {
				case sem <- struct{}{}:
				case <-ctx.Done():
					errc <- ctx.Err()
					return
				}
				if ins.Host == "" && len(ins.Endpoints) < 1 {
					errc <- fmt.Errorf("at least one endpoint expected per instruction, none given in %s", ins.Name)
					return
				}
				wg.Add(1)
				go func(ep string) {
					defer wg.Done()
					defer func() { <-sem }()

					// Copy the host information from endpoints.
					endpoint := il.Endpoints.URL(ep)
					if endpoint == "" {
						errc <- fmt.Errorf("no endpoint information found for endpoint %s", ep)
						return
					}
					ins.Host = endpoint

					// Execute.
					logrus.Infof("👉 executing instruction %s on target %s", ins.Name, ep)
					if err := execute(ctx, &ins); err != nil {
						errc <- fmt.Errorf("instruction %s: %w", ins.Name, err)
						return
					}
				}(ep)
			}

			wg.Wait()
		}
	}()

	return errc
}

// Do does stuff according to the instruction given.
func (e *Executor) Do(ctx context.Context, ins *Instruction) error {
	log := logrus.WithField("instruction", ins.Name)
	method, err := findHTTPMethod(ins.Method)
	if err != nil {
		return err
	}
	ins.Method = method

	var opts = make([]api.Opt, 0)
	client := e.cli

	// Override credentials if set in current instruction. If not set use the
	// credentials given in the CLI.
	authorization, err := client.Authorization()
	if err != nil {
		return fmt.Errorf("could not get authorization header: %w", err)
	}
	authParts := strings.Split(authorization, " ")
	if len(authParts) == 0 {
		return fmt.Errorf("no authorization header found")
	}
	var user, pass, bearerToken string
	var ok bool
	switch authParts[0] {
	case "Bearer":
		log.Debugf("switch to case bearer")
		bearerToken, ok = api.ParseBearerAuth(authorization)
		if ins.BearerToken != "" && ok {
			bearerToken = ins.BearerToken
			log.Debugf("overriding bearer token because new one has been set in instruction")
		}
		opts = append(opts, api.WithAuthorization(bearerToken))
	case "Basic":
		log.Debugf("switch to case basic")
		// If (for some reason) the client is set up to use basic authorization,
		// but the current instruction defines a bearer token ...
		if ins.BearerToken != "" {
			// ... we prefer to use the bearer token authorization for this
			// instruction.
			log.Debugf("but use bearer token authorization for instruction %s", ins.Name)
			opts = append(opts, api.WithAuthorization(ins.BearerToken))
		} else {
			var ok bool
			user, pass, ok = api.ParseBasicAuth(authorization)
			switch {
			case ins.Username != "" && ins.Password != "":
				user, pass = ins.Username, ins.Password
			case ins.Username != "" && ok:
				// Just change the username.
				user = ins.Username
				log.Debugf("overriding username because new one has been set in instruction: %s", user)
			case ins.Password != "" && ok:
				// Just change the password.
				pass = ins.Password
				log.Debugf("overriding password because new one has been set in instruction: %s", strings.Repeat("•", len(pass)))
			}
			opts = append(opts, api.WithBasicAuth(user, pass))
		}
	case "":
		log.Debugf("switch to case empty string")
		switch {
		case ins.BearerToken != "":
			opts = append(opts, api.WithAuthorization(ins.BearerToken))
		case ins.Username != "" && ins.Password != "":
			opts = append(opts, api.WithBasicAuth(ins.Username, ins.Password))
		}
	default:
		log.Debugf("switch to default case")
		return fmt.Errorf("unknown authorization scheme %s encountered", authParts[0])
	}

	// Now clone the HTTP client.
	client, err = client.Clone(opts...)
	if err != nil {
		return err
	}

	// In case the directive sets a different host, use it for the current instruction.
	if ins.Host != "" {
		if err := magnolia.WithBaseURL(ins.Host)(client); err != nil {
			return err
		}
	}

	if e.dryrun {
		log.Infof("dry-run: would execute %s %s, payload: %+v", method, ins.Path, ins.Data)
		return nil
	}

	// Handle idempotency.
	var execute jobFunc = basicJob
	if !ins.Force {
		switch method {
		case http.MethodDelete:
			execute = deleteJob
		case http.MethodPut:
			execute = func(ctx context.Context, client *magnolia.Client, ins *Instruction) error {
				if err := putJob(ctx, client, ins); err != nil {
					return err
				}
				return basicJob(ctx, client, ins)
			}
		}
	}

	if err := execute(ctx, client, ins); err != nil {
		return err
	}
	return nil
}

func findHTTPMethod(in string) (out string, err error) {
	switch strings.ToUpper(in) {
	case strings.ToUpper(http.MethodGet):
		return http.MethodGet, nil
	case strings.ToUpper(http.MethodPost):
		return http.MethodPost, nil
	case strings.ToUpper(http.MethodPut):
		return http.MethodPut, nil
	case strings.ToUpper(http.MethodDelete):
		return http.MethodDelete, nil
	}

	return "", errors.New("no valid method given (get, post, put, delete)")
}
