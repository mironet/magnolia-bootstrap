package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	authKeySecretName = "magnolia-auth-key"
	privatePemKey     = "private.pem"
	publicPemKey      = "public.pem"
)

var keySecretCmd = &cobra.Command{
	Use:   "createauthkey",
	Short: "Creates the key pair (used to authenticate requests to magnolia), stores the pair in a k8s secret and stores the public key at the specified location in the file system.",
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["createauthkey"]
		logw := logrus.StandardLogger().WriterLevel(logrus.TraceLevel)
		defer logw.Close()
		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		k8sCli, err := k8sClient(ctx)
		if err != nil {
			return fmt.Errorf("could not get kubernetes client: %v", err)
		}

		secret, err := k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Get(ctx, authKeySecretName, metav1.GetOptions{})
		if err != nil {
			if !errors.IsNotFound(err) {
				return fmt.Errorf("could not get secret %s: %w", authKeySecretName, err)
			}
			// Here we know that the auth key secret was not found.

			// Thus we create the key pair and set the secret.
			privatePem, publicPem, err := generateKeyPair()
			if err != nil {
				return fmt.Errorf("could not generate key pair: %w", err)
			}

			secret, err = createKeyPairSecret(ctx, privatePem, publicPem, k8sCli)
			if err != nil {
				return fmt.Errorf("could not create key pair secret: %w", err)
			}
		}
		logrus.Infof("successfully fetched secret %s", secret.Name)

		// Store public key at given path. (Fetch public pem from k8s secret).
		data, ok := secret.Data[publicPemKey]
		if !ok {
			return fmt.Errorf("secret %s must contain data with key %s", secret.Name, publicPemKey)
		}
		pubKeyPath := vip.GetString("public-key-path")
		err = os.WriteFile(pubKeyPath, data, 0644)
		if err != nil {
			return fmt.Errorf("could not write to file: %w", err)
		}

		logrus.Infof("successfully wrote public key to file at %s", pubKeyPath)
		return nil
	},
}

// createKeyPairSecret creates the auth key secret. Note: If the secret can not
// be created because it already exists, returns the existing secret.
func createKeyPairSecret(ctx context.Context, privatePem, publicPem []byte, k8sCli *k8sCli) (*v1.Secret, error) {
	secret := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      authKeySecretName,
			Namespace: k8sCli.namespaceName,
		},
		Data: map[string][]byte{
			publicPemKey:  publicPem,
			privatePemKey: privatePem,
		},
	}
	var err error
	secret, err = k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Create(ctx, secret, metav1.CreateOptions{})
	if err != nil {
		if errors.IsAlreadyExists(err) {
			return k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Get(ctx, authKeySecretName, metav1.GetOptions{})
		}
		return nil, fmt.Errorf("could not create secret: %w", err)
	}
	logrus.Infof("successfully created secret %s", secret.Name)
	return secret, nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["createauthkey"] = v

	flags := keySecretCmd.Flags()

	flags.String("public-key-path", "", "Path where the public key must be stored.")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(keySecretCmd)
}
