package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"mgnlboot/cmd/api"
	"mgnlboot/cmd/health"
	"mgnlboot/cmd/magnolia"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "start bootstrap in server mode",
	Args: func(cmd *cobra.Command, args []string) error {
		vip := vipers["serve"]
		target := vip.GetString("magnolia")
		if _, err := url.Parse(target); err != nil {
			return fmt.Errorf("could not parse URL %s: %w", target, err)
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["serve"]

		if vip.GetString("instructions") != "" {
			logrus.Warnf("instructions flag is deprecated: its content will be ignored")
		}
		if vip.GetDuration("max-wait") != defaultMaxDuration {
			logrus.Warnf("max-wait flag is deprecated: its content will be ignored")
		}

		verbosity := setVerbosity(vip.GetInt("verbosity"))
		logw := logrus.StandardLogger().WriterLevel(logrus.TraceLevel)
		defer logw.Close()
		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		insecure := vip.GetBool("insecure")
		mgnl := vip.GetString("magnolia")
		hcPath := vip.GetString("path")

		// Make sure that the raw base url used for the health check always ends
		// with a slash.
		if mgnl != "" && !strings.HasSuffix(mgnl, "/") {
			mgnl = mgnl + "/"
		}
		// Make sure the health check path never starts with a slash (if it is
		// not a slash only).
		if hcPath != "/" && strings.HasPrefix(hcPath, "/") {
			hcPath = strings.TrimPrefix(hcPath, "/")
		}

		cli, err := magnolia.NewClient(
			magnolia.OptFunc(api.WithBaseURL(mgnl)),
			magnolia.OptFunc(api.WithInsecure(insecure)),
			magnolia.OptFunc(api.WithVerbosity(verbosity)),
			magnolia.OptFunc(api.WithLogger(log.New(logw, "", 0))),
		)
		if err != nil {
			logrus.Errorf("could not get HTTP client: %s", err)
			return nil
		}
		codes := vip.GetStringSlice("code")
		response := vip.GetStringSlice("response")
		interval := vip.GetDuration("interval")
		maxInterval := vip.GetDuration("max-interval")
		timeout := vip.GetDuration("connect-timeout")

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		// This is our Magnolia health check.
		op := func() error {
			ctx, cancel := context.WithTimeout(ctx, timeout)
			defer cancel()
			err := httpGet(ctx, cli, hcPath, codes, response)
			if errors.Is(err, context.Canceled) || errors.Is(err, context.DeadlineExceeded) {
				return backoff.Permanent(err)
			}
			return err
		}

		var n int
		notify := func(err error, d time.Duration) {
			logrus.Debugf("error received, waiting for %s: %s", d, err)
			n++
		}

		healthOpts := []health.Opt{
			health.WithInterval(interval),
			health.WithMaxInterval(maxInterval),
			health.WithTimeout(vip.GetDuration("timeout")),
			health.WithMaxRetries(vip.GetInt("retry")),
			health.WithLogger(log.New(logrus.StandardLogger().WriterLevel(logrus.DebugLevel), "", 0)),
			health.WithInfoLogger(log.New(logrus.StandardLogger().WriterLevel(logrus.InfoLevel), "", 0)),
		}

		check, err := health.NewCheck(
			"magnolia health",
			op,
			append(healthOpts,
				health.WithNotify(notify),
			)...,
		)
		if err != nil {
			logrus.Fatalf("error creating health check: %v", err)
		}

		isEnabled := true
		// According to the magnolia-helm chart environment variable
		// MGNLBOOT_PUBLIC_SERVICE_NAME is only set if the bootstrapper is
		// enabled. Thus we can use it to determine whether the bootstrapper is
		// enabled. Note: The bootstrapper is considered to be enabled, if it
		// does more than just the simple health check.
		if os.Getenv(publicServiceNameEnvVar) == "" {
			isEnabled = false
			logrus.Infof("bootstrapper is not enabled")
		}

		var k8sCli *k8sCli
		var mgnlCli *magnolia.Client
		if isEnabled {
			var err error
			k8sCli, err = k8sClient(ctx)
			if err != nil {
				logrus.Errorf("could not get kubernetes client: %v", err)
				return nil
			}

			secretName := vip.GetString("magnolia-auth-secret-name")
			secret, err := k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Get(ctx, secretName, v1.GetOptions{})
			if err != nil {
				logrus.Fatalf("could not get secret %s: %v", secretName, err)
			}
			conf := magnolia.Config{
				UseJWTTokenIssuer: true,
				TokenLife:         vip.GetDuration("token-life"),
				EarlyRefreshBias:  vip.GetDuration("early-refresh-bias"),
				PrivateKey:        secret.Data[privatePemKey],
				Insecure:          vip.GetBool("insecure"),
				Mgnl:              vip.GetString("magnolia"),
				Endpoint:          vip.GetString("endpoint"),
				Verbosity:         setVerbosity(vip.GetInt("verbosity")),
			}
			mgnlCli, err = magnolia.NewClientFromConfig(conf)
			if err != nil {
				logrus.Fatalf("error getting new magnolia client from config: %s", err)
			}
		}

		address := vip.GetString("listen")
		keepReceiverNodes := vip.GetBool("keep-receiver-nodes")
		logrus.Infof("keepReceiverNodes is set to %v", keepReceiverNodes)
		deactivateReceiverCreation := vip.GetBool("deactivate-receiver-creation")
		logrus.Infof("deactivateReceiverCreation is set to %v", deactivateReceiverCreation)
		activationKeyPath := vip.GetString("activation-keypair-fullpath")

		srv, err := newServer(ctx,
			address,
			check,
			check,
			k8sCli,
			mgnlCli,
			isEnabled,
			keepReceiverNodes,
			deactivateReceiverCreation,
			activationKeyPath,
		)
		if err != nil {
			logrus.Fatalf("could not create server: %v", err)
		}

		if vip.GetBool("use-activation-proxy") {
			if err := srv.withActivationProxy(vip.GetString("magnolia")); err != nil {
				logrus.Fatalf("could not initialize activation proxy: %v", err)
			}
		}

		logrus.Infof("server version %s listening on %s", rootCmd.Version, address)
		go srv.Run()
		for err := range srv.Done {
			logrus.Error(err)
		}

		return nil
	},
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["serve"] = v

	flags := serveCmd.Flags()

	flags.StringP("listen", "l", ":8765", "Where we listen on")
	flags.StringP("magnolia", "m", "http://magnolia:8080", "Where to find our own Magnolia instance, used for the startup probe check")
	flags.StringP("endpoint", "e", "configuration", "Which API endpoint we're using")
	flags.String("username", "superuser", "User name for API requests")
	flags.String("password", "superuser", "Password for API requests")
	flags.String("magnolia-auth-secret-name", authKeySecretName, "Name of the secret where the magnolia auth key pair can be found.")
	flags.StringP("instructions", "i", "", "Instruction yaml file location (Deprecated: flag has no effect.)")
	flags.CountP("verbosity", "v", "Verbosity level (--v: verbose, --vv: debug, --vvv: trace)")
	flags.Bool("dry-run", false, "Dry run, don't change anything")
	flags.DurationP("max-wait", "w", defaultMaxDuration, "Max time to wait for completion of all tasks. (Deprecated: flag has no effect.)")
	flags.Bool("insecure", false, "Do not verify server's TLS certificates. WARNING: Use in production is not recommended!")

	// Flags for the health check.
	flags.StringSlice("code", []string{"200-399"}, "Healthcheck: Check for this HTTP response code range. Non-contiguous ranges can be separated by comma.")
	flags.IntP("retry", "r", -1, "Healthcheck: Retry this many times before giving up. Negative values mean 'infinite'.")
	flags.DurationP("interval", "n", time.Millisecond*500, "Healthcheck: Starting retry interval between failed attempts.")
	flags.Duration("max-interval", time.Second*10, "Healthcheck: Max retry interval between failed attempts.")
	flags.Duration("connect-timeout", time.Second*5, "Timeout for a single HTTP operation in probes.")
	flags.DurationP("timeout", "t", time.Second*30, "Timeout before transitioning from success->fail.")
	flags.StringSlice("response", nil, "Regex of response to expect. If all of the regexes match, the probe succeeds.")
	flags.String("path", "/", "Path used to do healthchecks against, e.g. '/.rest/status'")
	flags.Bool("retry-set-superuser", false, "Set to true if the bootstrapper should retry to set the superuser password until it succeeds.")
	flags.Bool("retry-set-publickey", false, "Set to true if the bootstrapper should retry to set the public key until it succeeds.")

	// JWT-related flags.
	flags.Duration("token-life", 5*time.Minute, "Duration between the jwt token issuance and its expiry.")
	flags.Duration("early-refresh-bias", 30*time.Second, "If the duration until expiry of the jwt token is inside this early refresh bias, a new token is issued for the next request to magnolia.")

	// Feature toggles.
	flags.Bool("keep-receiver-nodes", false, "Set this flag if the bootstrapper should not delete any public receiver nodes automatically. Note: This means that the public receivers must always be deleted manually.")
	flags.Bool("deactivate-receiver-creation", false, "Set this flag if the bootstrapper should not create any public receiver nodes automatically. Note: This means that the public receivers must always be created manually.")
	flags.BoolP("use-activation-proxy", "p", true, "Start activation endpoint proxy for host given in '-m'.")

	// Activation key stuff.
	flags.String("activation-keypair-fullpath", "", "Full path of the keypair properties file.")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(serveCmd)
}
