package cmd

import (
	"bytes"
	"context"
	"crypto"
	"crypto/rsa"
	"fmt"
	"mgnlboot/cmd/magnolia"
	"os"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	activationSecretNameEnvVar = "MGNLBOOT_ACTIVATION_SECRET_NAME"
	activationSecretKeyEnvVar  = "MGNLBOOT_ACTIVATION_SECRET_KEY"

	passwordKeyPathEnvVar    = "MGNLBOOT_PASSWORD_KEY_PATH"
	passwordSecretNameEnvVar = "MGNLBOOT_PASSWORD_SECRET_NAME"
	passwordSecretKey        = "magnolia-password-manager-keypair.properties"

	defaultActivationKeypairFileName = "magnolia-activation-keypair.properties"
	defaultActivationKeySize         = 4096
	legacyActivationKeySize          = 1024
)

// 1) Read secret name from env var MGNLBOOT_ACTIVATION_SECRET_NAME.
//
// 2) Create activation key pair secret if it does not exist. Note: Use 4096
// bits as a default, if Magnolia is not recent enough to support it, the
// bootstrapper will try to recreate it with 1024.
//
// 3) Write activation key pair into activation-keypair volume.
//
// 4) If there is a secret for the password app, write the password key into the
// key path indicated.
var activationKeyCmd = &cobra.Command{
	Use:   "activationkey",
	Short: "Creates the activation key pair, stores the pair in a k8s secret and at the specified location in the file system.",
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["activationkey"]
		activationKeypairPath := vip.GetString("activation-keypair-path")
		if activationKeypairPath == "" {
			return fmt.Errorf("flag activation-keypair-path must be set")
		}
		activationKeypairFilename := vip.GetString("activation-keypair-filename")
		if activationKeypairFilename == "" {
			activationKeypairFilename = defaultActivationKeypairFileName
		}

		logw := logrus.StandardLogger().WriterLevel(logrus.TraceLevel)
		defer logw.Close()
		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		k8sCli, err := k8sClient(ctx)
		if err != nil {
			return fmt.Errorf("could not get kubernetes client: %v", err)
		}

		secretName := os.Getenv(activationSecretNameEnvVar)
		if secretName == "" {
			return fmt.Errorf("env var %s must be set", activationSecretNameEnvVar)
		}
		activationSecretKey := os.Getenv(activationSecretKeyEnvVar)
		if activationSecretKey == "" {
			activationSecretKey = activationKeypairFilename
		}

		// Writes the secret to disk for author to access it.
		var writeToDisk = func(data []byte) error {
			fullFilePath := filepath.Join(activationKeypairPath, activationKeypairFilename)
			err := os.WriteFile(fullFilePath, data, 0600) // We store it writable so it can be updated in admincentral.
			if err != nil {
				return fmt.Errorf("could not write file %s: %w", fullFilePath, err)
			}
			logrus.Info("👉 successfully wrote activation secret to disk")
			return nil
		}

		// Check which Magnolia version we have. If it's >= 6.2.38 make
		// sure our RSA activation key is at least 4096 bits in size.
		// See issue #60 here:
		// https://gitlab.com/mironet/magnolia-helm/-/issues/60
		cur, err := magnolia.GetVersion(os.DirFS(magnoliaDataPath)) // This path is hardcoded in the helm chart.
		if err != nil {
			return fmt.Errorf("error getting the current Magnolia version: %w", err)
		}
		minVer := magnolia.MustFromString(bug8MagnoliaMinVersion)
		var activationKeySize = defaultActivationKeySize
		if cur.Less(minVer) {
			activationKeySize = legacyActivationKeySize
		}

		// Find the secret.
		secret, err := k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Get(ctx, secretName, metav1.GetOptions{})
		if err != nil {
			if !errors.IsNotFound(err) {
				return fmt.Errorf("could not get secret %s: %w", secretName, err)
			}
			logrus.Infof("activation secret %s not present, creating ...", secretName)
			// Here we know that the secret was not found. Create the activation
			// key pair secret.
			privateHexDer, publicHexDer, err := genActivationKeyPair(activationKeySize)
			if err != nil {
				return fmt.Errorf("could not generate activation key pair: %w", err)
			}
			data := generateSecretData(privateHexDer, publicHexDer)
			secret, err = k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Create(ctx, &corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name: secretName,
				},
				Data: map[string][]byte{
					activationSecretKey: data,
				},
			}, metav1.CreateOptions{})
			if err != nil {
				return fmt.Errorf("could not create activation key pair secret: %w", err)
			}
			logrus.Infof("successfully created secret %s", secret.Name)
			// Write key to file now for Magnolia author.
			if err := writeToDisk(data); err != nil {
				return err
			}
			return nil
		}
		if secret == nil {
			return fmt.Errorf("secret cannot be nil")
		}
		logrus.Infof("successfully fetched secret %s", secret.Name)

		data, ok := secret.Data[activationSecretKey]
		if !ok {
			return fmt.Errorf("secret %s in namespace %s has no data key %s", secret.Name, secret.Namespace, activationSecretKey)
		}
		// Resets the key, the data var and the secret in k8s.
		var recreateKey = func(size int) error {
			privateHexDer, publicHexDer, err := genActivationKeyPair(size)
			if err != nil {
				return fmt.Errorf("could not generate activation key pair: %w", err)
			}
			data = generateSecretData(privateHexDer, publicHexDer)
			secret.Data[activationSecretKey] = data
			// This update in k8s should trigger watchers in public instances.
			_, err = k8sCli.cliSet.CoreV1().Secrets(k8sCli.namespaceName).Update(ctx, secret, metav1.UpdateOptions{})
			if err != nil {
				return fmt.Errorf("could not update activation secret: %w", err)
			}
			return nil
		}
		// Check if key size meets the minimum size for the Magnolia version in
		// use.
		privKeyHex, err := extractPrivateKey(string(data))
		if err != nil {
			return fmt.Errorf("could not read private key from data: %w", err)
		}
		var keysize int
		privKey, err := keyFromHex(privKeyHex)
		if err != nil {
			// Check if private and public key have been mixed up and if yes,
			// recreate the key. If not, return the original error.
			_, kerr := checkPrivateAndPublicKeyMixup(data)
			if kerr != nil {
				return fmt.Errorf("could not convert hex key to private key: %w", err)
			}
			// It looks like the private and public key have been mixed up.
			// Recreate the key.
			keysize = 0 // Setting this to 0 triggers a key recreation below.
		} else {
			rsaKey, ok := privKey.(*rsa.PrivateKey)
			if !ok {
				return fmt.Errorf("key of type %T unsupported", privKey)
			}
			// The size is reported in bytes, but we want bits.
			keysize = rsaKey.Size() * 8
		}
		if cur.Less(minVer) {
			// Stored key size must be <= 1024 and >= 512 for older Magnolia
			// versions.
			if keysize < 512 || keysize > 1024 {
				if err := recreateKey(legacyActivationKeySize); err != nil {
					return fmt.Errorf("could not recreate key: %w", err)
				}
				logrus.Infof("👉 recreated activation key, mgnl version = %s, old size = %d, new size = %d",
					cur.String(),
					keysize,
					legacyActivationKeySize,
				)
			}
		} else {
			// Stored key size must be >= 4096.
			if keysize < 4096 {
				if err := recreateKey(defaultActivationKeySize); err != nil {
					return fmt.Errorf("could not recreate key: %w", err)
				}
				logrus.Infof("👉 recreated activation key, mgnl version = %s, old size = %d, new size = %d",
					cur.String(),
					keysize,
					defaultActivationKeySize,
				)
			}
		}
		// Write key to file now for Magnolia author.
		if err := writeToDisk(data); err != nil {
			return err
		}

		// Check if have magnolia version >= 6.2.44 and if so, write the
		// password key to disk.
		v6_2_44 := magnolia.MustFromString("6.2.44")
		if !cur.Less(v6_2_44) {
			if err := handlePasswordKey(ctx, k8sCli); err != nil {
				return err
			}
		}
		return nil
	},
}

func handlePasswordKey(ctx context.Context, cli *k8sCli) error {
	passwordKeyPath := filepath.Join(os.Getenv(passwordKeyPathEnvVar), passwordSecretKey)
	if passwordKeyPath == "" {
		logrus.Infof("password key path not set in env var, skipping ...")
		return nil
	}
	passwordSecretName := os.Getenv(passwordSecretNameEnvVar)
	if passwordSecretName == "" {
		logrus.Info("password secret name not set in env var, skipping ...")
		return nil
	}
	// Find the secret.
	secret, err := cli.cliSet.CoreV1().Secrets(cli.namespaceName).Get(ctx, passwordSecretName, metav1.GetOptions{})
	if err != nil {
		if !errors.IsNotFound(err) {
			return fmt.Errorf("could not get secret %s: %w", passwordSecretName, err)
		}
		logrus.Infof("password secret %s not present: skipping sync to file ...", passwordSecretName)
		if _, err := cli.cliSet.CoreV1().Secrets(cli.namespaceName).Create(ctx, &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name: passwordSecretName,
			},
			Data: map[string][]byte{
				passwordSecretKey: {},
			},
		}, metav1.CreateOptions{}); err != nil {
			return fmt.Errorf("could not create empty password secret: %w", err)
		}
		logrus.Infof("successfully created empty password secret %s", passwordSecretName)
		return nil
	}
	if secret == nil {
		return fmt.Errorf("secret cannot be nil")
	}
	logrus.Infof("successfully fetched secret %s", secret.Name)

	data, ok := secret.Data[passwordSecretKey]
	if !ok {
		return fmt.Errorf("secret %s in namespace %s has no data key %s", secret.Name, secret.Namespace, passwordSecretKey)
	}
	if len(bytes.TrimSpace(data)) == 0 {
		logrus.Infof("password secret %s's data is empty: skipping sync to file ...", passwordSecretName)
		return nil
	}
	err = os.WriteFile(passwordKeyPath, data, 0600) // We store it writable in case we want to update it later.
	if err != nil {
		return fmt.Errorf("could not write file %s: %w", passwordKeyPath, err)
	}
	logrus.Infof("👉 successfully wrote password key to disk")
	return nil
}

var (
	testNow time.Time
)

func generateSecretData(private, public string) []byte {
	out := make([]byte, 0)
	now := time.Now()
	if !testNow.IsZero() {
		now = testNow
	}
	date := now.Format("02.Jan.2006")
	unixdate := now.Format("Mon Jan 02 15:04:05 MST 2006")
	out = append(out, []byte(fmt.Sprintf("#generated %s by %s\n", date, "magnolia-bootstrap"))...)
	out = append(out, []byte(fmt.Sprintf("#%s\n", unixdate))...)
	out = append(out, []byte(fmt.Sprintf("key.private=%s\n", private))...)
	out = append(out, []byte(fmt.Sprintf("key.public=%s\n", public))...)
	return out
}

// Checks if the private and public key have been mixed up in the secret data,
// comparing the "key.private=" and "key.public=" fields inside the data. If
// there was a mixup, the private key is returned.
func checkPrivateAndPublicKeyMixup(data []byte) (crypto.PrivateKey, error) {
	privateKey, err := extractPrivateKey(string(data))
	if err != nil {
		return nil, fmt.Errorf("could not extract private key: %w", err)
	}
	publicKey, err := extractPubKey(string(data))
	if err != nil {
		return nil, fmt.Errorf("could not extract public key: %w", err)
	}
	privKey, err := keyFromHex(privateKey)
	if err != nil {
		// Let's see if we can get the private key from the public key data.
		privKey, err = keyFromHex(publicKey)
		if err != nil {
			// If it's still an error, we have just garbage as the input.
			return nil, fmt.Errorf("could not convert hex key to private key: %w", err)
		}
		// We could, so there was a mixup. Return the private key and no error.
		return privKey, nil
	}
	// No error, so just return the private key. This should not be the case
	// since we usually test for that condition before we enter this function.
	return privKey, nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["activationkey"] = v

	flags := activationKeyCmd.Flags()

	flags.String("activation-keypair-path", "", "Path of directory for the keypair properties.")
	flags.String("activation-keypair-filename", "", "Filename of the keypair properties.")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(activationKeyCmd)
}
