package cmd

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"mgnlboot/cmd/api"
	"mgnlboot/cmd/magnolia"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func setVerbosity(v int) int {
	var verbosity int
	switch v {
	case verbosityInfo:
		verbosity = api.ModeInfo
		logrus.SetLevel(logrus.InfoLevel)
	case verbosityDebug:
		verbosity = api.ModeDebug
		logrus.SetLevel(logrus.DebugLevel)
	case verbosityTrace:
		verbosity = api.ModeTrace
		logrus.SetLevel(logrus.TraceLevel)
	}
	return verbosity
}

var httpGetCmd = &cobra.Command{
	Use:   "httpget",
	Short: "Issue HTTP GET request and check for return codes, for readinessProbes/livenessProbes",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("target URL required")
		}
		var err error
		if _, err = url.Parse(args[0]); err != nil {
			return fmt.Errorf("could not parse URL %s: %w", args[0], err)
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["httpGet"]
		insecure := vip.GetBool("insecure")
		verbosity := setVerbosity(vip.GetInt("verbosity"))

		logw := logrus.StandardLogger().WriterLevel(logrus.TraceLevel)
		defer logw.Close()
		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		cli, err := magnolia.NewClient(
			magnolia.OptFunc(api.WithBaseURL(args[0])),
			magnolia.OptFunc(api.WithInsecure(insecure)),
			magnolia.OptFunc(api.WithVerbosity(verbosity)),
			magnolia.OptFunc(api.WithLogger(log.New(logw, "", 0))),
		)
		if err != nil {
			logrus.Errorf("could not get HTTP client: %s", err)
			return nil
		}
		codes := vip.GetStringSlice("code")
		response := vip.GetStringSlice("response")
		interval := vip.GetDuration("interval")
		maxInterval := vip.GetDuration("max-interval")
		retry := vip.GetInt("retry")
		hcPath := vip.GetString("path")
		var k int

		ctx, cancel := context.WithTimeout(context.Background(), vip.GetDuration("timeout"))
		defer cancel()

		op := func() error {
			err := httpGet(ctx, cli, hcPath, codes, response)
			if err == nil {
				return nil
			}
			if errors.Is(err, context.Canceled) || errors.Is(err, context.DeadlineExceeded) {
				// Permanent error.
				return backoff.Permanent(err)
			}
			var e net.Error
			if errors.As(err, &e) {
				if e.Timeout() || e.Temporary() {
					logrus.Debugf("temporary or timeout error, retrying: %v", err)
				}
			}
			return err
		}
		notify := func(err error, d time.Duration) {
			k++
			logrus.Debugf("error during try #%d: %v", k, err)
			logrus.Debugf("sleeping for %s before retrying ...", d)
		}

		var bo backoff.BackOff
		b := backoff.NewExponentialBackOff()
		b.InitialInterval = interval
		b.MaxInterval = maxInterval
		bo = b
		if retry > 0 {
			bo = backoff.WithMaxRetries(b, uint64(retry))
		}
		boctx := backoff.WithContext(bo, ctx)
		if err := backoff.RetryNotify(op, boctx, notify); err != nil {
			logrus.Error(err)
			os.Exit(1)
		}

		return nil
	},
}

func httpGet(ctx context.Context, cli *magnolia.Client, path string, codeRange, response []string) error {
	codes, err := parseCodeRanges(codeRange)
	if err != nil {
		return err
	}
	codes = codes.Compact()
	logrus.Debugf("checking for code ranges: %s", codes)
	req, err := cli.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return err
	}
	logrus.Debugf("sending request to: %s", req.URL)
	var body bytes.Buffer
	resp, err := cli.Do(ctx, req, &body)
	if err != nil {
		var e *api.ErrorResponse
		if !errors.As(err, &e) {
			return err
		}
		resp = e.Response
	}
	if !codes.Between(resp.StatusCode) {
		return fmt.Errorf("status code = %d, expected range = %s", resp.StatusCode, codes)
	}
	logrus.Debugf("received status code: %d", resp.StatusCode)

	// If we have response regex checks to do, now is the time.
	if len(response) > 0 {
		for _, expr := range response {
			regex, err := regexp.Compile(expr)
			if err != nil {
				return fmt.Errorf("error compiling response regex: %w", err)
			}
			if !regex.Match(body.Bytes()) {
				return fmt.Errorf("regexp %q did not match response content", expr)
			}
		}
	}
	logrus.Debugf("success")
	return nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["httpGet"] = v

	flags := httpGetCmd.Flags()

	flags.Bool("insecure", false, "Do not verify server's TLS certificates. WARNING: Use in production is not recommended!")
	flags.CountP("verbosity", "v", "Verbosity level (--v: verbose, --vv: debug, --vvv: trace)")
	flags.StringSlice("code", []string{"200-399"}, "Check for this HTTP response code range. Non-contiguous ranges can be separated by comma.")
	flags.String("path", "/", "Path used to do healthchecks against, e.g. '/.rest/status'")

	flags.IntP("retry", "r", -1, "Retry this many times before giving up. Negative values mean 'infinite'.")
	flags.DurationP("interval", "i", time.Millisecond*500, "Starting retry interval between failed attempts.")
	flags.DurationP("max-interval", "m", time.Second*10, "Max retry interval between failed attempts.")
	flags.DurationP("timeout", "t", time.Minute*5, "Timeout for the whole operation (including retries).")
	flags.StringSlice("response", nil, "Regex of response to expect. If all of the regexes match, the probe succeeds.")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(httpGetCmd)
}
