package api

import (
	"crypto/rsa"
	"fmt"
	"log"
	"sync"
	"time"

	jwt "github.com/dgrijalva/jwt-go/v4"
)

type jwtTokenIssuer struct {
	mu               sync.Mutex
	token            *jwt.Token
	signedToken      string
	earlyRefreshBias time.Duration
	privateKey       *rsa.PrivateKey
	tokenLife        time.Duration
	issuer           string
	subject          string
	audience         string
}

func NewJWTTokenIssuer(tokenLife, earlyRefreshBias time.Duration, privateKey []byte, issuer, subject, audience string) (*jwtTokenIssuer, error) {
	key, err := jwt.ParseRSAPrivateKeyFromPEM(privateKey)
	if err != nil {
		return nil, fmt.Errorf("could not parse rsa private key: %w", err)
	}
	token, err := newToken(time.Now(), tokenLife, issuer, subject, audience)
	if err != nil {
		return nil, fmt.Errorf("could not get new token: %w", err)
	}
	signedToken, err := signToken(token, key)
	if err != nil {
		return nil, fmt.Errorf("could not sign given token with given key: %w", err)
	}
	return &jwtTokenIssuer{
		token:            token,
		signedToken:      signedToken,
		earlyRefreshBias: earlyRefreshBias,
		privateKey:       key,
		tokenLife:        tokenLife,
		issuer:           issuer,
		subject:          subject,
		audience:         audience,
	}, nil
}

// BearerToken returns the signed bearer token string of the stored jwt token.
// If that token is close to expiry, a new token is created, stored, signed and
// returned. This is done in a go-routine-safe way.
func (p *jwtTokenIssuer) BearerToken(log *log.Logger) (string, error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	// Get the token expiry.
	exp, err := p.tokenExpiry()
	if err != nil {
		return "", fmt.Errorf("could not get token expiry: %w", err)
	}

	// If the token expires soon, get a new token and sign it.
	if expiresSoon(time.Now(), exp, p.earlyRefreshBias) {
		if log != nil {
			log.Printf("jwt token about to expire (at %s), issuing a new one...", exp.String())
		}
		token, err := newToken(time.Now(), p.tokenLife, p.issuer, p.subject, p.audience)
		if err != nil {
			return "", fmt.Errorf("could not get new token: %w", err)
		}
		signedToken, err := signToken(token, p.privateKey)
		if err != nil {
			return "", fmt.Errorf("could not sign token: %w", err)
		}

		// If we have a new token AND it has been signed successfully, update
		// both stored values.
		p.token = token
		p.signedToken = signedToken
		if log != nil {
			log.Printf("successfully issued a new jwt token")
		}
	}

	return fmt.Sprintf("Bearer %s", p.signedToken), nil
}

func expiresSoon(t, expiry time.Time, earlyRefreshBias time.Duration) bool {
	return t.After(expiry.Add(-earlyRefreshBias))
}

func newToken(t time.Time, tokenLife time.Duration, issuer, subject, audience string) (*jwt.Token, error) {
	claimStrings, err := jwt.ParseClaimStrings(audience)
	if err != nil {
		return nil, fmt.Errorf("could not parse claim strings: %w", err)
	}

	claims := jwt.StandardClaims{
		Issuer:    issuer,
		Subject:   subject,
		Audience:  claimStrings,
		ExpiresAt: jwt.At(t.Add(tokenLife)),
		NotBefore: jwt.At(t),
		IssuedAt:  jwt.At(t),
	}
	return jwt.NewWithClaims(jwt.SigningMethodRS256, claims), nil
}

func signToken(token *jwt.Token, key *rsa.PrivateKey) (string, error) {
	if token == nil {
		return "", fmt.Errorf("token cannot be nil")
	}

	signed, err := token.SignedString(key)
	if err != nil {
		return "", err
	}

	return signed, nil
}

func (p *jwtTokenIssuer) tokenExpiry() (time.Time, error) {
	claims, err := p.claims()
	if err != nil {
		return time.Time{}, fmt.Errorf("could not get claims from token: %w", err)
	}
	if claims.ExpiresAt == nil {
		return time.Time{}, fmt.Errorf("claims.ExpiresAt cannot be nil")
	}
	return claims.ExpiresAt.Time, nil
}

func (p *jwtTokenIssuer) claims() (jwt.StandardClaims, error) {
	stdClaims, ok := p.token.Claims.(jwt.StandardClaims)
	if !ok {
		return jwt.StandardClaims{}, fmt.Errorf("expected jwt.MapClaims, got %T", p.token.Claims)
	}
	return stdClaims, nil
}
