package api

import (
	"testing"
	"time"
)

func Test_expiresSoon(t *testing.T) {
	now := time.Now()
	type args struct {
		t                time.Time
		expiry           time.Time
		earlyRefreshBias time.Duration
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "expires soon",
			args: args{
				t:                now,
				expiry:           now.Add(15 * time.Second),
				earlyRefreshBias: 30 * time.Second,
			},
			want: true,
		},
		{
			name: "not expires soon",
			args: args{
				t:                now,
				expiry:           now.Add(time.Minute),
				earlyRefreshBias: 30 * time.Second,
			},
			want: false,
		},
		{
			name: "already expired",
			args: args{
				t:                now,
				expiry:           now.Add(-15 * time.Second),
				earlyRefreshBias: 30 * time.Second,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := expiresSoon(tt.args.t, tt.args.expiry, tt.args.earlyRefreshBias); got != tt.want {
				t.Errorf("expiresSoon() = %v, want %v", got, tt.want)
			}
		})
	}
}
