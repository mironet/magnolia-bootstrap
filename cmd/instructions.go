package cmd

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"mgnlboot/cmd/api"
	"mgnlboot/cmd/magnolia"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

// Instruction tells the API what to do.
type Instruction struct {
	Name        string   `yaml:"name"`                   // Name of the instruction, required.
	Method      string   `yaml:"method,omitempty"`       // HTTP method used in this instruction.
	Host        string   `yaml:"host,omitempty"`         // Host in case we want to switch hosts in between executions.
	Endpoints   []string `yaml:"endpoints,omitempty"`    // List of endpoints (hosts) defined in the instruction list.
	Username    string   `yaml:"username,omitempty"`     // Override username for this request.
	Password    string   `yaml:"password,omitempty"`     // Override password for this request.
	BearerToken string   `yaml:"bearer_token,omitempty"` // Set the Authorization bearer token to this value. Overrides basic auth (username, password).
	Path        string   `yaml:"path,omitempty"`         // The target path.

	Force        bool          `yaml:"force,omitempty"`         // Force update/delete, do not check for idempotency.
	AllowFailure bool          `yaml:"allow_failure,omitempty"` // Allow this instruction to fail.
	WaitFor      time.Duration `yaml:"wait_for,omitempty"`      // Wait this long for the command to succeed/fail.
	RetryOn      intRangeList  `yaml:"retry_on,omitempty"`      // Retry on those HTTP return codes. Other codes outside of 200-399 are considered as errors.
	Retry        int           `yaml:"retry,omitempty"`         // Retry this many times.

	Data *magnolia.Node `yaml:"data,omitempty"` // The instruction data.

	Register string `yaml:"register,omitempty"` // Register output from this instruction to a var with this name. Not yet implemented!
	When     string `yaml:"when,omitempty"`     // If this go template expression evaluates to true, this instruction is executed. Not yet implemented!
}

// InstructionList is a list of instructions.
type InstructionList struct {
	Endpoints Endpoints     `yaml:"endpoints,omitempty"` // Map of endpoint name to URL.
	Playbook  []Instruction `yaml:"playbook,omitempty"`  // Our list of instructions.
}

// Endpoints is a list of endpoints.
type Endpoints []Endpoint

// URL returns the url for the named endpoint, empty string in case there's no
// endpoint with this name.
func (e Endpoints) URL(name string) string {
	for _, v := range e {
		if v.Name == name {
			return v.URL
		}
	}
	return ""
}

// Endpoint describes a remote host.
type Endpoint struct {
	Name string `yaml:"name,omitempty"` // The name of the endpoint.
	URL  string `yaml:"url,omitempty"`  // The url of the endpoint.
}

func getInstructionList(list *InstructionList, in io.Reader) error {
	var buf bytes.Buffer
	if err := templateThis(&buf, in); err != nil {
		return err
	}
	if err := yaml.NewDecoder(&buf).Decode(list); err != nil {
		return fmt.Errorf("could not parse yaml file generated from template: %w", err)
	}
	return nil
}

type jobFunc func(ctx context.Context, client *magnolia.Client, ins *Instruction) error

// This is basically what any job does at the end.
func basicJob(ctx context.Context, client *magnolia.Client, ins *Instruction) error {
	req, err := client.NewRequest(ctx, ins.Method, ins.Path, ins.Data)
	if err != nil {
		return fmt.Errorf("could not create HTTP request: %w", err)
	}
	if _, err := client.Do(ctx, req, nil); err != nil {
		if ins.AllowFailure {
			logrus.Debugf("error executing %s, but allow_failure = true: %v", ins.Name, err)
			return nil
		}
		return fmt.Errorf("error during HTTP request: %w", err)
	}
	return nil
}

// A delete job, idempotent (404 is not an error).
func deleteJob(ctx context.Context, client *magnolia.Client, ins *Instruction) error {
	log := logrus.WithField("instruction", ins.Name)
	log.Debugf("checking for existence of %s before deleting", ins.Path)
	req, err := client.NewRequest(ctx, http.MethodGet, ins.Path, nil)
	if err != nil {
		return fmt.Errorf("could not check for existence before execution: %w", err)
	}
	var node magnolia.Node
	if _, err := client.Do(ctx, req, &node); err != nil {
		var e *api.ErrorResponse
		if errors.As(err, &e) {
			if e.Response.StatusCode == http.StatusNotFound {
				log.Debugf("node %s doesn't exist, doing nothing", ins.Path)
				// Return because we have nothing to delete.
				return nil
			}
		}
		return fmt.Errorf("could not check for existence before execution: %w", err)
	}
	return basicJob(ctx, client, ins)
}

func putJob(ctx context.Context, client *magnolia.Client, ins *Instruction) error {
	log := logrus.WithField("instruction", ins.Name)
	if ins.Data == nil {
		// This request is basically invalid. A put should always have a
		// Data field.
		return fmt.Errorf("data field of PUT instruction should not be empty: %+v", ins)
	}
	newpath := fmt.Sprintf("%s/%s", ins.Path, ins.Data.Name)
	log.Debugf("checking for existence of %s before PUT-ing", newpath)
	req, err := client.NewRequest(ctx, http.MethodGet, newpath, nil)
	if err != nil {
		return fmt.Errorf("could not check for existence before execution: %w", err)
	}
	var node magnolia.Node
	if _, err := client.Do(ctx, req, &node); err != nil {
		var e *api.ErrorResponse
		if errors.As(err, &e) {
			if e.Response.StatusCode == http.StatusNotFound {
				log.Debugf("node %s not found, using PUT request to path %s", newpath, ins.Path)
				return nil
			}
		}

		log.Errorf("could not check for existence before execution: %s", err)
		return fmt.Errorf("could not check for existence before execution: %w", err)
	}

	log.Debugf("Found node %s, using POST request instead of PUT", newpath)
	ins.Path = newpath
	ins.Method = http.MethodPost
	return nil
}
