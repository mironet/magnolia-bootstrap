package cmd

import (
	"crypto"
	"crypto/md5"
	"crypto/rsa"
	"encoding/hex"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestGenerateSecretData(t *testing.T) {
	testNow = time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		private string
		public  string
		want    []byte
	}{
		{
			private: "private_key",
			public:  "public_key",
			want: []byte("#generated 01.Jan.2022 by magnolia-bootstrap\n" +
				"#Sat Jan 01 00:00:00 UTC 2022\n" +
				"key.private=private_key\n" +
				"key.public=public_key\n"),
		},
		{
			private: "another_private_key",
			public:  "another_public_key",
			want: []byte("#generated 01.Jan.2022 by magnolia-bootstrap\n" +
				"#Sat Jan 01 00:00:00 UTC 2022\n" +
				"key.private=another_private_key\n" +
				"key.public=another_public_key\n"),
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("private=%s,public=%s", tt.private, tt.public), func(t *testing.T) {
			got := generateSecretData(tt.private, tt.public)
			if diff := cmp.Diff(got, tt.want); diff != "" {
				t.Error(diff)
			}
		})
	}
}

func TestCheckPrivateAndPublicKeyMixup(t *testing.T) {
	type wantFunc func(crypto.PrivateKey) error
	tests := []struct {
		name    string
		data    []byte
		want    wantFunc
		wantErr bool
		errMsg  string
	}{
		{
			name: "correct private and public key",
			data: []byte(fixture("activation-key.secret", t)),
			want: func(pk crypto.PrivateKey) error {
				switch v := pk.(type) {
				case *rsa.PrivateKey:
					// Check a few things.
					if v.Size()*8 != 1024 {
						return fmt.Errorf("key size != 1024: %d", v.Size()*8)
					}
					// Check modulus.
					// Get the md5 sum of the modulus.
					sum := md5.Sum(v.N.Bytes())
					hexsum := hex.EncodeToString(sum[:])
					if hexsum != "24945631aba75b28c2b05a65596ecd91" {
						return fmt.Errorf("unexpected key modulus: %s", hexsum)
					}
				default:
					return fmt.Errorf("unexpected key type %T", pk)
				}
				return nil
			},
			wantErr: false,
			errMsg:  "",
		},
		{
			name: "mixed up private and public key",
			data: []byte(fixture("activation-key-mixup.secret", t)),
			want: func(pk crypto.PrivateKey) error {
				switch v := pk.(type) {
				case *rsa.PrivateKey:
					// Check a few things.
					if v.Size()*8 != 1024 {
						return fmt.Errorf("key size != 1024: %d", v.Size()*8)
					}
					// Check modulus.
					// Get the md5 sum of the modulus.
					sum := md5.Sum(v.N.Bytes())
					hexsum := hex.EncodeToString(sum[:])
					if hexsum != "24945631aba75b28c2b05a65596ecd91" {
						return fmt.Errorf("unexpected key modulus: %s", hexsum)
					}
				default:
					return fmt.Errorf("unexpected key type %T", pk)
				}
				return nil
			},
			wantErr: false,
			errMsg:  "",
		},
		{
			name:    "invalid private key",
			data:    []byte(fixture("activation-key-invalid.secret", t)),
			want:    nil,
			wantErr: true,
			errMsg:  "could not convert hex key to private key: could not parse private key DER encoded bytes (PKCS8): asn1: syntax error: data truncated",
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKey, err := checkPrivateAndPublicKeyMixup(tt.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("checkPrivateAndPublicKeyMixup() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil && err.Error() != tt.errMsg {
				t.Errorf("checkPrivateAndPublicKeyMixup() error message = %v, wantErrMsg %v", err.Error(), tt.errMsg)
				return
			}
			if tt.want != nil {
				if err := tt.want(gotKey); err != nil {
					t.Error(err)
				}
			}
		})
	}
}
