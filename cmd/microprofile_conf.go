package cmd

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/magiconair/properties"
)

const (
	maxSupportedReceivers = 100
)

type microprofileConf struct {
	receivers  []publishingReceiver
	properties *properties.Properties
}

func (c *microprofileConf) updateReceivers() error {
	if c.properties == nil {
		return fmt.Errorf("properties not initialized")
	}
	for i, r := range c.receivers {
		if _, _, err := c.properties.Set(fmt.Sprintf("magnolia.publishing.receivers[%d].name", i), r.name); err != nil {
			return fmt.Errorf("could not set name for receiver %s: %w", r.name, err)
		}
		if _, _, err := c.properties.Set(fmt.Sprintf("magnolia.publishing.receivers[%d].url", i), r.url); err != nil {
			return fmt.Errorf("could not set url for receiver %s: %w", r.name, err)
		}
		if _, _, err := c.properties.Set(fmt.Sprintf("magnolia.publishing.receivers[%d].enabled", i), fmt.Sprintf("%t", r.enabled)); err != nil {
			return fmt.Errorf("could not set enabled for receiver %s: %w", r.name, err)
		}
	}
	for i := len(c.receivers); i < maxSupportedReceivers; i++ {
		c.properties.Delete(fmt.Sprintf("magnolia.publishing.receivers[%d].name", i))
		c.properties.Delete(fmt.Sprintf("magnolia.publishing.receivers[%d].url", i))
		c.properties.Delete(fmt.Sprintf("magnolia.publishing.receivers[%d].enabled", i))
	}
	return nil
}

func (c *microprofileConf) MarshalText() ([]byte, error) {
	if err := c.updateReceivers(); err != nil {
		return nil, fmt.Errorf("could not insert receivers into properties: %w", err)
	}
	buf := new(bytes.Buffer)
	_, err := c.properties.WriteComment(buf, "# ", properties.UTF8)
	if err != nil {
		return nil, fmt.Errorf("could not write comment: %w", err)
	}
	return buf.Bytes(), nil
}

func (c *microprofileConf) extractReceivers() {
	c.receivers = make([]publishingReceiver, 0)
	for i := 0; i < maxSupportedReceivers; i++ {
		name := strings.TrimSpace(c.properties.GetString(fmt.Sprintf("magnolia.publishing.receivers[%d].name", i), ""))
		if name == "" {
			break
		}
		enabledStr := strings.TrimSpace(c.properties.GetString(fmt.Sprintf("magnolia.publishing.receivers[%d].enabled", i), ""))
		c.receivers = append(c.receivers, publishingReceiver{
			name:    name,
			url:     strings.TrimSpace(c.properties.GetString(fmt.Sprintf("magnolia.publishing.receivers[%d].url", i), "")),
			enabled: enabledStr == "true",
		})
	}
}

func (c *microprofileConf) UnmarshalText(text []byte) error {
	var err error
	c.properties, err = properties.LoadString(string(text))
	if err != nil {
		return fmt.Errorf("could not load microprofile properties from string: %w", err)
	}
	c.extractReceivers()
	return nil
}
