package cmd

import (
	"reflect"
	"testing"

	"github.com/google/gopacket/bytediff"
	"github.com/magiconair/properties"
)

const (
	exampleMicroprofileDataContent = `magnolia.publishing.receivers[0].name=magnoliaPublic8080-0
magnolia.publishing.receivers[0].url=http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[0].enabled=true
magnolia.publishing.receivers[1].name=magnoliaPublic8080-1
magnolia.publishing.receivers[1].url=http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[1].enabled=true`
	exampleMicroprofileDataContent2 = `    magnolia.publishing.receivers[0].name=magnoliaPublic8080-0	
	magnolia.publishing.receivers[0].url=http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/ 
magnolia.publishing.receivers[0].enabled=true 
magnolia.publishing.receivers[1].name=magnoliaPublic8080-1
magnolia.publishing.receivers[1].url=http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/ 
magnolia.publishing.receivers[1].enabled=true 
	`
	exampleMicroprofileDataContent3 = `    magnolia.publishing.receivers[0].name=magnoliaPublic8080-0	
	# some irrelevant comment
	magnolia.publishing.receivers[0].url=http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/ 
magnolia.publishing.receivers[0].enabled=true 
magnoila.publishing.test
magnolia.publishing.receivers[1].name=magnoliaPublic8080-1
magnolia.publishing.receivers[1].url=http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/ 
magnolia.publishing.receivers[1].enabled=true 
kgydsafhblkncxm blnmotewrikzuhopijgglkybnkvknlk
	`
	exampleMicroprofileDataContentWithComments = `# First receiver
magnolia.publishing.receivers[0].name = magnoliaPublic8080-0
magnolia.publishing.receivers[0].url = http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[0].enabled = true

# Second receiver
magnolia.publishing.receivers[1].name = magnoliaPublic8080-1
magnolia.publishing.receivers[1].url = http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[1].enabled = true
`
)

func Test_microprofileConf_UnmarshalText(t *testing.T) {
	tests := []struct {
		name    string
		text    []byte
		wantErr bool
		want    []publishingReceiver
	}{
		{
			name:    "success",
			text:    []byte(exampleMicroprofileDataContent),
			wantErr: false,
			want: []publishingReceiver{
				{
					name:    "magnoliaPublic8080-0",
					url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
				{
					name:    "magnoliaPublic8080-1",
					url:     "http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
			},
		},
		{
			name:    "robust with spaces",
			text:    []byte(exampleMicroprofileDataContent2),
			wantErr: false,
			want: []publishingReceiver{
				{
					name:    "magnoliaPublic8080-0",
					url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
				{
					name:    "magnoliaPublic8080-1",
					url:     "http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
			},
		},
		{
			name:    "robust with irrelevant lines",
			text:    []byte(exampleMicroprofileDataContent3),
			wantErr: false,
			want: []publishingReceiver{
				{
					name:    "magnoliaPublic8080-0",
					url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
				{
					name:    "magnoliaPublic8080-1",
					url:     "http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
			},
		},
		{
			name:    "robust with spaces around equal sign",
			text:    []byte(exampleMicroprofileDataContentWithComments),
			wantErr: false,
			want: []publishingReceiver{
				{
					name:    "magnoliaPublic8080-0",
					url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
				{
					name:    "magnoliaPublic8080-1",
					url:     "http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/",
					enabled: true,
				},
			},
		},
		{
			name:    "fail with invalid line",
			text:    []byte("magnolia.publishing.receivers(0).enabled=true"),
			wantErr: false,
			want:    []publishingReceiver{},
		},
		{
			name:    "fail with invalid line",
			text:    []byte("magnolia.publishing.receivers[0].test=true"),
			wantErr: false,
			want:    []publishingReceiver{},
		},
		{
			name:    "fail with invalid line",
			text:    []byte("magnolia.publishing.receivers[0].blablabla"),
			wantErr: false,
			want:    []publishingReceiver{},
		},
		{
			name:    "fail with invalid line",
			text:    []byte("magnolia.publishing.receivers[a].enabled=true"),
			wantErr: false,
			want:    []publishingReceiver{},
		},
		{
			name:    "fail only 100 receivers are supported",
			text:    []byte("magnolia.publishing.receivers[100].enabled=true"),
			wantErr: false,
			want:    []publishingReceiver{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &microprofileConf{}
			err := c.UnmarshalText(tt.text)
			if (err != nil) != tt.wantErr {
				t.Errorf("microprofileConf.UnmarshalText() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err != nil {
				t.Logf("error: %v", err)
			}
			if !reflect.DeepEqual(c.receivers, tt.want) {
				t.Errorf("microprofileConf.UnmarshalText() = %v, want %v", *c, tt.want)
			}
		})
	}
}

func Test_microprofileConf_MarshalText(t *testing.T) {
	p1, err := properties.LoadString(exampleMicroprofileDataContentWithComments)
	if err != nil {
		t.Fatalf("could not load properties: %v", err)
	}
	p2, err := properties.LoadString(exampleMicroprofileDataContentWithComments)
	if err != nil {
		t.Fatalf("could not load properties: %v", err)
	}
	p3, err := properties.LoadString(exampleMicroprofileDataContentWithComments)
	if err != nil {
		t.Fatalf("could not load properties: %v", err)
	}
	p4, err := properties.LoadString(exampleMicroprofileDataContentWithComments)
	if err != nil {
		t.Fatalf("could not load properties: %v", err)
	}

	type fields struct {
		receivers  []publishingReceiver
		properties *properties.Properties
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				receivers: []publishingReceiver{
					{
						name:    "magnoliaPublic8080-0",
						url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
					{
						name:    "magnoliaPublic8080-1",
						url:     "http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
				},
				properties: p1,
			},
			want:    []byte(exampleMicroprofileDataContentWithComments),
			wantErr: false,
		},
		{
			name: "add success",
			fields: fields{
				receivers: []publishingReceiver{
					{
						name:    "magnoliaPublic8080-0",
						url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
					{
						name:    "magnoliaPublic8080-1",
						url:     "http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
					{
						name:    "magnoliaPublic8080-2",
						url:     "http://pruning-molly-magnolia-helm-public-2.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
				},
				properties: p2,
			},
			want: []byte(`# First receiver
magnolia.publishing.receivers[0].name = magnoliaPublic8080-0
magnolia.publishing.receivers[0].url = http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[0].enabled = true

# Second receiver
magnolia.publishing.receivers[1].name = magnoliaPublic8080-1
magnolia.publishing.receivers[1].url = http://pruning-molly-magnolia-helm-public-1.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[1].enabled = true
magnolia.publishing.receivers[2].name = magnoliaPublic8080-2
magnolia.publishing.receivers[2].url = http://pruning-molly-magnolia-helm-public-2.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[2].enabled = true
`),
		},
		{
			name: "update success",
			fields: fields{
				receivers: []publishingReceiver{
					{
						name:    "magnoliaPublic8080-0",
						url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
					{
						name:    "test",
						url:     "http://test.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: false,
					},
				},
				properties: p3,
			},
			want: []byte(`# First receiver
magnolia.publishing.receivers[0].name = magnoliaPublic8080-0
magnolia.publishing.receivers[0].url = http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[0].enabled = true

# Second receiver
magnolia.publishing.receivers[1].name = test
magnolia.publishing.receivers[1].url = http://test.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[1].enabled = false
`),
		},
		{
			name: "delete success",
			fields: fields{
				receivers: []publishingReceiver{
					{
						name:    "magnoliaPublic8080-0",
						url:     "http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/",
						enabled: true,
					},
				},
				properties: p4,
			},
			want: []byte(`# First receiver
magnolia.publishing.receivers[0].name = magnoliaPublic8080-0
magnolia.publishing.receivers[0].url = http://pruning-molly-magnolia-helm-public-0.pruning-molly-magnolia-helm-public-svc:8080/
magnolia.publishing.receivers[0].enabled = true
`),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &microprofileConf{
				receivers:  tt.fields.receivers,
				properties: tt.fields.properties,
			}
			got, err := c.MarshalText()
			if (err != nil) != tt.wantErr {
				t.Errorf("microprofileConf.MarshalText() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("microprofileConf.MarshalText() = %v, want %v", string(got), string(tt.want))
				t.Error(bytediff.BashOutput.String(bytediff.Diff(got, tt.want)))
			}
		})
	}
}
