package cmd

import (
	"context"
	"fmt"
	"mgnlboot/cmd/api"
	"mgnlboot/cmd/magnolia"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	verbosityWarn = iota
	verbosityInfo
	verbosityDebug
	verbosityTrace
)

var bootCmd = &cobra.Command{
	Use:   "boot",
	Short: "bootstrap magnolia",
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["boot"]
		magnoliaAddress := vip.GetString("magnolia")
		if magnoliaAddress == "" {
			return fmt.Errorf("magnolia address of server should be set, none set")
		}
		endpoint := vip.GetString("endpoint")

		warnStr := "only do this if instructions should use bearer token authorization"
		username := vip.GetString("username")
		if username == "" {
			logrus.Warnf("username was not provided: %s", warnStr)
		}

		password := vip.GetString("password")
		if password == "" {
			logrus.Warnf("password was not provided: %s", warnStr)
		}

		instrFile := vip.GetString("instructions")
		if instrFile == "" {
			return fmt.Errorf("no instruction file provided")
		}

		verbosity := setVerbosity(vip.GetInt("verbosity"))

		dryrun := vip.GetBool("dry-run")
		insecure := vip.GetBool("insecure")

		var ctx = context.Background()
		dur := vip.GetDuration("max-wait")
		ctx, cancel := context.WithTimeout(ctx, dur)
		defer cancel()

		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		opts := []magnolia.Opt{magnolia.WithEndpoint(endpoint),
			magnolia.WithBaseURL(magnoliaAddress),
			magnolia.OptFunc(api.WithVerbosity(verbosity)),
			magnolia.OptFunc(api.WithInsecure(insecure)),
		}

		// Only append basic auth information if it has actually been set.
		if username != "" && password != "" {
			opts = append(opts, magnolia.OptFunc(api.WithBasicAuth(username, password)))
		}

		// First get a new client.
		client, err := magnolia.NewClient(opts...)
		if err != nil {
			return fmt.Errorf("could not initialize magnolia client: %w", err)
		}

		// Get content of yaml input with modifications.
		file, err := os.Open(instrFile)
		if err != nil {
			return fmt.Errorf("could not open instructions file: %w", err)
		}
		defer file.Close()

		var inst InstructionList
		if err := getInstructionList(&inst, file); err != nil {
			return fmt.Errorf("could not get instructions from file: %w", err)
		}

		executor := NewExecutor(client, &inst, dryrun)
		if err := executor.Run(ctx); err != nil {
			logrus.Fatalf("❌ error during execution: %s", err)
		}

		logrus.Infof("🥳 successfully executed all instructions given")
		return nil
	},
}

var (
	// Minutes bootup time should be enough for Magnolia.
	defaultMaxDuration = time.Minute * 15
)

func init() {
	v := viper.New()
	initViper(v)
	vipers["boot"] = v

	flags := bootCmd.Flags()

	flags.StringP("magnolia", "m", "http://magnolia:8080", "Where to find Magnolia instance")
	flags.StringP("endpoint", "e", "nodes/v1", "Which API endpoint we're using")
	flags.String("username", "superuser", "User name for API requests")
	flags.String("password", "superuser", "Password for API requests")
	flags.StringP("instructions", "i", "", "Instruction yaml file location")
	flags.CountP("verbosity", "v", "Verbosity level (--v: verbose, --vv: debug, --vvv: trace)")
	flags.Bool("dry-run", false, "Dry run, don't change anything")
	flags.DurationP("max-wait", "w", defaultMaxDuration, "Max time to wait for completion of all tasks.")
	flags.Bool("insecure", false, "Do not verify server's TLS certificates. WARNING: Use in production is not recommended!")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(bootCmd)
}
