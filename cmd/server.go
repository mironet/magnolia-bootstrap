package cmd

import (
	"bytes"
	"context"
	"crypto/rsa"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mgnlboot/cmd/health"
	"mgnlboot/cmd/magnolia"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/fsnotify/fsnotify"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/semaphore"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apimachinery/pkg/watch"

	_ "net/http/pprof"
)

const (
	urlQueryParamUser = "user"
	urlQueryParamPW   = "pw"

	handleSuperuserPasswordTimeout = time.Minute
	handleReenableSuperuserTimeout = time.Minute

	bug8MagnoliaMinVersion = "6.2.38" // https://gitlab.com/mironet/magnolia-bootstrap/-/issues/8
	magnoliaVersion6_2_44  = "6.2.44" // https://docs.magnolia-cms.com/product-docs/6.2/releases/release-notes-for-magnolia-cms-6.2.44/#_improvements
	magnoliaVersion6_3_0   = "6.3.0"
	magnoliaDataPath       = "/magnolia" // This is a hardcoded internal path in magnolia-helm.
)

type server struct {
	Shutdown context.CancelFunc
	Done     chan error // Closed when we're done.

	ctx                        context.Context
	address                    string // Where we listen on.
	srv                        *http.Server
	r                          *mux.Router
	readinessCheck             *health.Check   // The current status as by the readiness check.
	livenessCheck              *health.Check   // Check if the Magnolia instance is live.
	al                         *ActivationLock // The lock collection holding activation locks.
	k8sCli                     *k8sCli
	mgnlCli                    *magnolia.Client
	isEnabled                  bool
	deactivateReceiverDeletion bool
	deactivateReceiverCreation bool

	activationSync fileSecretSync
	passwordSync   fileSecretSync

	currentMagnoliaVersion magnolia.Version // The current Magnolia version.
}

type fileSecretSync struct {
	fileKeySem *semaphore.Weighted // Protects file key access.
	syncKey    chan interface{}    // Send an empty msg here to update the file from secrets in instances.

	fileKeyPath string // Full path to the file key.
	secretName  string // Name of the secret.
	secretKey   string // Data key of the secret. Required to access the data value containing the file content.
}

func newServer(ctx context.Context, address string, livez, readyz *health.Check, k8sCli *k8sCli, mgnlCli *magnolia.Client,
	isEnabled, keepReceiverNodes, deactivateReceiverCreation bool, activationKeyPath string) (*server, error) {
	// If the bootstrapper is enabled, we need to make sure that activation
	// secret name and activation secret key are set.
	activationSecretName := os.Getenv(activationSecretNameEnvVar)
	activationSecretKey := os.Getenv(activationSecretKeyEnvVar)
	if isEnabled {
		if activationKeyPath == "" {
			activationKeyPath = defaultActivationKeypairFileName
		}
		if activationSecretName == "" {
			return nil, fmt.Errorf("env var %s must be set", activationSecretNameEnvVar)
		}
		if activationSecretKey == "" {
			// Don't use full path as secret key, just the filename.
			activationSecretKey = filepath.Base(activationKeyPath)
		}
	}

	// Get the currently used magnolia version.
	cur, err := magnolia.GetVersion(os.DirFS(magnoliaDataPath)) // This path is hardcoded in the helm chart.
	if err != nil {
		return nil, fmt.Errorf("error getting the current Magnolia version: %w", err)
	}
	v6_2_44 := magnolia.MustFromString(magnoliaVersion6_2_44)
	isAtLeast6_2_44 := !cur.Less(v6_2_44)

	// If the bootstrapper is enabled and the magnolia version is at least
	// 6.2.44, we need to make sure that password secret name and password
	// secret key are set.
	passwordSecretName := os.Getenv(passwordSecretNameEnvVar)
	passwordKeyPath := os.Getenv(passwordKeyPathEnvVar)
	if isEnabled && isAtLeast6_2_44 {
		if passwordSecretName == "" {
			return nil, fmt.Errorf("env var %s must be set", passwordSecretNameEnvVar)
		}
		if passwordKeyPath == "" {
			return nil, fmt.Errorf("env var %s must be set", passwordKeyPathEnvVar)
		}
		passwordKeyPath = filepath.Join(passwordKeyPath, passwordSecretKey)
	}

	ctx, cancel := context.WithCancel(ctx)
	s := &server{
		Shutdown:                   cancel,
		Done:                       make(chan error, 2),
		ctx:                        ctx,
		address:                    address,
		readinessCheck:             readyz,
		livenessCheck:              livez,
		al:                         NewActivationLock(),
		k8sCli:                     k8sCli,
		mgnlCli:                    mgnlCli,
		isEnabled:                  isEnabled,
		deactivateReceiverDeletion: keepReceiverNodes,
		deactivateReceiverCreation: deactivateReceiverCreation,
		activationSync: fileSecretSync{
			syncKey:     make(chan interface{}),
			fileKeyPath: activationKeyPath,
			fileKeySem:  semaphore.NewWeighted(1),
			secretName:  activationSecretName,
			secretKey:   activationSecretKey,
		},
		passwordSync: fileSecretSync{
			syncKey:     make(chan interface{}),
			fileKeyPath: passwordKeyPath,
			fileKeySem:  semaphore.NewWeighted(1),
			secretName:  passwordSecretName,
			secretKey:   passwordSecretKey,
		},
		currentMagnoliaVersion: *cur,
	}

	r := mux.NewRouter()
	r.HandleFunc("/healthz", s.handleHealthz()).Methods(http.MethodGet)
	r.HandleFunc("/readyz", s.handleReadyz()).Methods(http.MethodGet)
	r.HandleFunc("/livez", s.handleLivez()).Methods(http.MethodGet)
	r.HandleFunc("/startz", s.handleStartz()).Methods(http.MethodGet)
	r.HandleFunc("/superuser-password", s.handleSuperuserPassword()).Methods(http.MethodPost)
	r.HandleFunc("/reenable-superuser", s.handleReenableSuperuser()).Methods(http.MethodPost)
	r.HandleFunc("/sync-activationkey", s.handleSyncActivationKey()).Methods(http.MethodPost)
	r.PathPrefix("/debug/").Handler(http.DefaultServeMux)

	logw := logrus.StandardLogger().WriterLevel(logrus.DebugLevel) // Don't clutter logs.
	lr := handlers.LoggingHandler(logw, r)

	srv := &http.Server{
		Handler:      lr,
		Addr:         address,
		WriteTimeout: time.Hour * 24, // This is just a safety net.
		ReadTimeout:  time.Hour * 24, // This is just a safety net.
	}
	s.srv = srv
	s.r = r

	return s, nil
}

// Run the server asynchronously.
func (s *server) Run() {
	go func() {
		defer close(s.Done)

		// Define new error group.
		initErrGroup, initErrGroupCtx := errgroup.WithContext(s.ctx)

		if s.isEnabled {
			// Define the exponential backoff function.
			var bo backoff.BackOff = func() backoff.BackOff {
				b := backoff.NewExponentialBackOff()
				return b
			}()

			// Set default base url, superuser password and public key with retry.
			if err := s.setDefaultBaseURL(initErrGroup, initErrGroupCtx, bo); err != nil {
				logrus.Errorf("could not set default base url: %v", err)
			}
			if err := s.setSuperuserPWWithRetry(initErrGroup, initErrGroupCtx, bo); err != nil {
				logrus.Errorf("could not set superuser password: %v", err)
			}

			// Sync password key file to secret.
			if s.passwordSync.fileKeyPath != "" {
				go func() {
					for {
						select {
						case <-s.ctx.Done():
							return // Stop if parent ctx is done.
						default:
						}
						s.watchFile(s.passwordSync, bo)
						time.Sleep(time.Second)
					}
				}()
			}

			switch s.k8sCli.instanceType {
			case instanceTypeAuthor:
				// Sync activation secret to file.
				if s.activationSync.fileKeyPath != "" {
					go func() {
						for {
							select {
							case <-s.ctx.Done():
								return // Stop if parent ctx is done.
							default:
							}
							s.watchSecret(s.activationSync, func() error {
								return s.writeSecretToDisk(s.activationSync, bo)
							})
							time.Sleep(time.Second)
						}
					}()
				} else {
					logrus.Errorf("activation key path should not be empty, cannot sync")
				}

				// Sync activation key to secret.
				go func() {
					for {
						select {
						case <-s.ctx.Done():
							return // Stop if parent ctx is done.
						default:
						}
						s.watchFile(s.activationSync, bo)
						time.Sleep(time.Second)
					}
				}()

				// We need to watch for public instance endpoints and update the
				// public receiver nodes accordingly. (But we do this only if it
				// is not the case that both the public receiver creation and
				// deletion have been deactivated.)
				if s.deactivateReceiverCreation && s.deactivateReceiverDeletion {
					break
				}

				// Prepare the endpoint label selector.
				headlessMirrorReq, err := labels.NewRequirement(labelKeyHeadlessMirror, selection.Exists, []string{})
				if err != nil {
					logrus.Errorf("could not construct headless mirror requirement: %v", err)
					return
				}
				serviceSelector := labels.NewSelector()
				serviceSelector = serviceSelector.Add(*headlessMirrorReq)
				serviceOpts := metav1.ListOptions{
					LabelSelector: serviceSelector.String(),
				}

				go func() {
					for /*ever*/ {
						select {
						case <-s.ctx.Done():
							return // Stop if parent ctx is done.
						default:
						}

						// Watch local endpoints.
						watcherList := make(watcherList, 0)
						localWatch, err := s.k8sCli.cliSet.CoreV1().Endpoints(s.k8sCli.namespaceName).Watch(s.ctx, metav1.ListOptions{
							FieldSelector: fields.OneTermEqualSelector(metav1.ObjectNameField, s.k8sCli.serviceName).String(),
						})
						if err != nil {
							logrus.Errorf("could not watch local endpoints: %s", err)
						}
						if err == nil {
							watcherList = append(watcherList, localWatch)
						}

						// Watch remote services.
						remoteWatch, err := s.k8sCli.cliSet.CoreV1().Services(s.k8sCli.namespaceName).Watch(s.ctx, serviceOpts)
						if err != nil {
							logrus.Errorf("could not watch remote endpoints: %s", err)
						}
						if err == nil {
							watcherList = append(watcherList, remoteWatch)
						}

						// Start watching public instance endpoints and pods.
						s.watchPublicInstanceEndpoints(watcherList)
						time.Sleep(time.Second)
					}
				}()
			case instanceTypePublic:
				// Watch for changes in the activation secret and update the
				// public key if anything happens.
				go func() {
					for {
						select {
						case <-s.ctx.Done():
							return // Stop if parent ctx is done.
						default:
						}
						// Public instances only need the public key and thus we
						// only watch the secret in k8s, there's no mounted
						// secret anywhere.
						s.watchSecret(s.activationSync, func() error {
							return s.setPublicKeyWithRetry(bo)
						})
						time.Sleep(time.Second) // Wait a bit before restarting watchers.
					}
				}()
			}
		}

		var wg sync.WaitGroup
		var listenAndServeErr error
		wg.Add(1)
		go func() {
			defer wg.Done()
			listenAndServeErr = s.srv.ListenAndServe()
		}()
		if err := initErrGroup.Wait(); err != nil {
			logrus.Errorf("error during bootstrapper initialization: %v", err)
		}
		wg.Wait()
		s.Done <- listenAndServeErr
	}()

	go func() {
		defer logrus.Infof("shutting down server ...")
		<-s.ctx.Done()
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		if err := s.srv.Shutdown(ctx); err != nil {
			logrus.Errorf("error while shutting down: %s", err)
		}

	}()

	go s.readinessCheck.Run(s.ctx) // Starts embedded liveness check too.
}

func (s *server) watchFile(sync fileSecretSync, bo backoff.BackOff) {
	// Start watching the file and sync changes to secret.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logrus.Errorf("error setting up file watcher: %v", err)
		return
	}
	defer func() {
		watcher.Close()
		logrus.Info("stopping to watch file")
	}()
	if err := watcher.Add(sync.fileKeyPath); err != nil {
		logrus.Warnf("error trying to watch path %s: %v", sync.fileKeyPath, err)
		// We can continue without watching the file, because we ...
	}
	// ... also add the parent directory to watch for create events if the file is
	// deleted and recreated for some reason (i.e. troubleshooting)
	dir := filepath.Dir(sync.fileKeyPath)
	if err := watcher.Add(dir); err != nil {
		logrus.Errorf("error trying to watch dir: %s", dir)
	}

	for {
		select {
		case <-s.ctx.Done():
			return // Stop if parent ctx is done.
		case ev, ok := <-watcher.Events:
			if !ok {
				logrus.Debugf("fsnotify done")
				return
			}
			var doSync bool
			if ev.Has(fsnotify.Remove) {
				// Remove the watcher since the file is gone. We probably don't
				// need to do this, but just to be safe. This is also why an
				// error trying this results in a debug log only.
				logrus.Debugf("file %s has been removed ...", ev.Name)
				if err := watcher.Remove(sync.fileKeyPath); err != nil {
					logrus.Debugf("error removing file watcher: %v", err)
				}
				doSync = false
			}
			if ev.Has(fsnotify.Write) || ev.Has(fsnotify.Create) {
				doSync = true
			}
			if ev.Has(fsnotify.Create) {
				logrus.Debugf("file %s has been created just now, attaching watcher", ev.Name)
				if err := watcher.Add(ev.Name); err != nil {
					logrus.Debugf("error re-adding file watcher: %v", err)
				}
			}
			if !doSync {
				continue
			}
			// Sync the file to the secret.
			if err := s.writeFileToSecret(sync, bo); err != nil {
				logrus.Debugf("error writing file to secret: %v", err)
				continue
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				logrus.Debugf("fsnotify done")
				return
			}
			logrus.Debugf("fsnotify error: %v", err)
		}
	}
}

func (s *server) watchSecret(sync fileSecretSync, execute func() error) {
	// Read secret name from environment.
	watcher, err := s.k8sCli.cliSet.CoreV1().Secrets(s.k8sCli.namespaceName).Watch(s.ctx, metav1.ListOptions{
		FieldSelector: fmt.Sprintf("metadata.name=%s", sync.secretName), // https://github.com/kubernetes/kubernetes/issues/53459
	})
	logrus.Infof("starting to watch secret %s", sync.secretName)
	if err != nil {
		logrus.Errorf("could not set up secret watcher to monitor %s: %v", sync.secretName, err)
		return
	}
	defer func() {
		watcher.Stop()
		logrus.Infof("stopping to watch secret %s", sync.secretName)
	}()

	for {
		select {
		case <-s.ctx.Done():
			return // Stop if parent ctx is done.
		case event, more := <-watcher.ResultChan():
			if !more {
				// Channel is closed. We should restart the watchers.
				return
			}
			switch event.Type {
			case watch.Added, watch.Modified:
				logrus.Infof("secret %s: received event type %s", sync.secretName, event.Type)
				if err := execute(); err != nil {
					logrus.Debugf("could not execute action: %v", err)
					continue
				}
			case watch.Deleted:
				// Uh-oh. Log this event, and wait for better times.
				logrus.Warnf("secret %s has been deleted ... waiting for its reappearance ...", sync.secretName)
			}
		// Manual sync from http endpoint.
		case <-sync.syncKey:
			logrus.Infof("secret %s: manual sync", sync.secretName)
			if err := execute(); err != nil {
				logrus.Debugf("could execute action (manual sync): %v", err)
				continue
			}
		}
	}
}

func (s *server) watchPublicInstanceEndpoints(watcher watch.Interface) {
	logrus.Infof("starting to watch public instance endpoints")
	defer watcher.Stop()

	v6_3_0 := magnolia.MustFromString(magnoliaVersion6_3_0)
	isAtLeast6_3_0 := !s.currentMagnoliaVersion.Less(v6_3_0)

	// Define and use a buffered channel to buffer incoming events.
	bufferedEventCh := make(chan watch.Event, 5)
	go func() {
		for event := range watcher.ResultChan() {
			select {
			case bufferedEventCh <- event:
				// Send event to buffered channel.
			default:
				//  Ignore incoming events that don't fit into the buffer.
				logrus.Infof("public instance endpoints watcher: ignoring event of type %s: event queue is full", string(event.Type))
			}
		}
		logrus.Infof("public instance endpoints watch channel has been closed")
		close(bufferedEventCh)
	}()

	// Create and aquire a new (weighted) semaphore.
	sem := semaphore.NewWeighted(1)
	if err := sem.Acquire(s.ctx, 1); err != nil {
		logrus.Infof("error acquiring semaphore: %v", err)
		// Make sure buffered event channel is empty, before returning.
		for event := range bufferedEventCh {
			logrus.Infof("after error acquiring semaphore: ignoring event of type %s", string(event.Type))
		}
		return
	}

	// Asynchronously start to reconcile events from the buffered channel.
	go func() {
		defer sem.Release(1)
		// Read all events from the buffered channel.
		for event := range bufferedEventCh {
			switch event.Type {
			// If the event is of a type we are interested in.
			case watch.Added, watch.Modified, watch.Deleted:
				// In case of a "deleted" event, wait for a few seconds before
				// reconciling the receivers. This is to prevent receivers from
				// being gone for a short time when resources are being deleted
				// and immediately recreated by automated processes.
				if event.Type == watch.Deleted {
					time.Sleep(5 * time.Second)
				}
				err := backoff.Retry(errorLogWrapper("reconciling receivers", func() error {
					var err error
					if isAtLeast6_3_0 {
						err = s.reconcileReceiversV6_3()
					} else {
						err = s.reconcileReceiversV6_2()
					}
					if err != nil {
						outErr := fmt.Errorf("public instance endpoints watcher: could not reconcile receivers: %w", err)
						logrus.Error(outErr)
						return outErr
					}
					logrus.Infof("reconciled receivers successfully")
					return nil
				}), backoff.NewExponentialBackOff())
				if err != nil {
					logrus.Error(fmt.Errorf("stopped retry of reconcile receivers, final error is: %w", err))
				}
			default:
				logrus.Infof("public instance endpoints watcher received event of type %s: ignoring...", string(event.Type))
			}
		}
	}()

	// Don't return before the goroutine has finished.
	if err := sem.Acquire(s.ctx, 1); err != nil {
		logrus.Infof("error acquiring semaphore: %v", err)
	}
}

func (s *server) setDefaultBaseURL(g *errgroup.Group, gCtx context.Context, bo backoff.BackOff) error {
	// Read default base url from environment.
	defaultBaseURL := os.Getenv(defaultBaseUrlEnvVar)

	// Set default base url with retry.
	g.Go(func() error {
		err := backoff.Retry(errorLogWrapper("setting default base url", func() error {
			err := s.mgnlCli.SetDefaultBaseURL(gCtx, defaultBaseURL)
			if err != nil {
				if errors.Is(err, context.Canceled) {
					return backoff.Permanent(err)
				}
			}
			return err
		}), bo)
		return err
	})

	return nil
}

// errSuccess is an error wrapper which only logs the embedded error but should
// otherwise not be considered as one. It is used if an action isn't performed,
// but not because of an error.
type errSuccess struct {
	error
}

func (e errSuccess) Unwrap() error {
	return e.error
}

func errorLogWrapper(taskName string, retryFunc func() error) func() error {
	return func() error {
		err := retryFunc()
		if err != nil {
			var e errSuccess
			if errors.As(err, &e) {
				logrus.Debugf("not %s: %v", taskName, err)
				return nil
			}
			// Only log error message if the error is not a permanent backoff
			// error on the outside.
			var er *backoff.PermanentError
			if !errors.As(err, &er) {
				logrus.Debugf("error %s: %v, retrying...", taskName, err)
			}
		} else {
			logrus.Infof("👍 success: %s", taskName)
		}
		return err
	}
}

var (
	errMgnlSyncNotRequired = fmt.Errorf("activation key in magnolia and secret are equal, no sync required")
	errSyncNotRequired     = fmt.Errorf("file on disk and secret equal, no sync required")
	errSyncBusy            = fmt.Errorf("another process is busy updating or accessing the file")
)

func (s *server) writeFileToSecret(sync fileSecretSync, bo backoff.BackOff) error {
	// Synchronize with other goroutines accessing the file.
	if !sync.fileKeySem.TryAcquire(1) {
		return errSyncBusy
	}
	defer sync.fileKeySem.Release(1)
	return backoff.Retry(errorLogWrapper(
		fmt.Sprintf("writing file %s to secret %s", sync.fileKeyPath, sync.secretName),
		func() error {
			want, err := os.ReadFile(sync.fileKeyPath)
			if err != nil {
				return fmt.Errorf("could not read data from file %s: %w", sync.fileKeyPath, err)
			}
			// Compare file data and data in current secret, if present.
			secret, err := s.k8sCli.cliSet.CoreV1().Secrets(s.k8sCli.namespaceName).Get(s.ctx, sync.secretName, metav1.GetOptions{})
			if err != nil {
				// If the secret is gone, this is clearly a misconfiguration and
				// can be remediated by restarting the author instance for now.
				return fmt.Errorf("could not get secret %s: %w", sync.secretName, err)
			}
			have := secret.Data[sync.secretKey]
			if bytes.Equal(have, want) {
				// We are done here. Secret does contain the same data already.
				return errSuccess{errSyncNotRequired}
			}
			// Update the secret with the changed data.
			secret.Data[sync.secretKey] = want
			_, err = s.k8sCli.cliSet.CoreV1().Secrets(s.k8sCli.namespaceName).Update(s.ctx, secret, metav1.UpdateOptions{})
			if err != nil {
				return fmt.Errorf("could not update secret %s: %w", sync.secretName, err)
			}
			return nil
		}),
		bo,
	)
}

func (s *server) writeSecretToDisk(sync fileSecretSync, bo backoff.BackOff) error {
	// Synchronize with other goroutines accessing the file.
	if !sync.fileKeySem.TryAcquire(1) {
		return errSyncBusy
	}
	defer sync.fileKeySem.Release(1)
	return backoff.Retry(errorLogWrapper(
		fmt.Sprintf("writing secret %s to disk", sync.secretName),
		func() error {
			secretData, err := s.getSecretData(sync.secretName, sync.secretKey)
			if err != nil {
				return fmt.Errorf("could not get secret from k8s: %w", err)
			}
			// Compare secret data and data in current file, if present.
			ok, err := shouldWriteFile(sync.fileKeyPath, secretData)
			if err != nil {
				return fmt.Errorf("could not read file %s for comparison before write: %w", sync.fileKeyPath, err)
			}
			if !ok {
				// We are done here. File does contain the same data already.
				return errSuccess{errSyncNotRequired}
			}
			// Update the file with the changed data.
			if err := os.WriteFile(sync.fileKeyPath, secretData, 0600); err != nil {
				return fmt.Errorf("could not write secret %s to file: %w", sync.secretName, err)
			}
			return nil
		}),
		bo,
	)
}

// Returns true if the file at path does not contain want.
func shouldWriteFile(path string, want []byte) (bool, error) {
	have, err := os.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			return true, nil
		}
		return false, err
	}
	return !bytes.Equal(have, want), nil
}

func (s *server) setPublicKeyWithRetry(bo backoff.BackOff) error {
	// Set public key with retry.
	return backoff.Retry(errorLogWrapper(
		"setting public key",
		func() error {
			// Read secret.
			keypairData, err := s.getSecretData(s.activationSync.secretName, s.activationSync.secretKey)
			if err != nil {
				return fmt.Errorf("could not get secret from k8s: %w", err)
			}

			// Extract public key.
			wantPublicKeyHex, err := extractPubKey(string(keypairData))
			if err != nil {
				return fmt.Errorf("could not extract public key: %w", err)
			}
			wantPublicKey, err := pubkeyFromHex(wantPublicKeyHex)
			if err != nil {
				return fmt.Errorf("could not convert public key from hex: %w", err)
			}
			// Check if the public key is a valid RSA public key.
			if _, ok := wantPublicKey.(*rsa.PublicKey); !ok {
				return fmt.Errorf("public key is not a valid RSA public key, got type %T instead", wantPublicKey)
			}

			// (Try to) check what public key is set.
			havePublicKey, err := s.mgnlCli.GetPublicKey(s.ctx)
			if err != nil {
				logrus.Debugf("during setPublicKeyWithRetry: could not check what public key we have: %v", err)
			}
			if havePublicKey == wantPublicKeyHex {
				return errSuccess{errMgnlSyncNotRequired}
			}

			// Set the public key we want (if required).
			err = s.mgnlCli.SetPublicKey(s.ctx, wantPublicKeyHex)
			if err != nil {
				if errors.Is(err, context.Canceled) {
					return backoff.Permanent(err)
				}
			}
			return err
		},
	),
		bo,
	)
}

func (s *server) getSecretData(secretName, secretKey string) ([]byte, error) {
	secret, err := s.k8sCli.cliSet.CoreV1().Secrets(s.k8sCli.namespaceName).Get(s.ctx, secretName, metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("could not get secret %s in namespace %s: %w", s.k8sCli.namespaceName, secretName, err)
	}
	if secret == nil {
		return nil, fmt.Errorf("secret cannot be nil")
	}
	secretData, ok := secret.Data[secretKey]
	if !ok {
		return nil, fmt.Errorf("secret %s in namespace %s has no data key %s", secret.Name, secret.Namespace, secretKey)
	}
	return secretData, nil
}

func (s *server) setSuperuserPWWithRetry(g *errgroup.Group, gCtx context.Context, bo backoff.BackOff) error {
	// Fetch desired superuser name & superuser secret name.
	superuserSecretName, err := s.k8sCli.superuserSecretName()
	if err != nil {
		return fmt.Errorf("could not get superuser secret name: %w", err)

	}
	superuserName := os.Getenv(superuserNameEnvVar)
	if superuserName == "" {
		return fmt.Errorf("environment variable %s must be set", superuserNameEnvVar)
	}

	g.Go(func() error {
		var setSuperuserPW bool
		var superuserSecret = &v1.Secret{}
		var pw []byte
		if err := backoff.Retry(
			errorLogWrapper(
				"checking superuser password",
				func() error {
					// Get desired superuser password from secret.
					superuserSecret, err = s.k8sCli.cliSet.CoreV1().Secrets(s.k8sCli.namespaceName).Get(s.ctx, superuserSecretName, metav1.GetOptions{})
					if err != nil {
						return fmt.Errorf("could not get superuser secret: %w", err)
					}
					var ok bool
					pw, ok = superuserSecret.Data["password"]
					if !ok {
						return fmt.Errorf("no data with key password found in secret %s", superuserSecretName)
					}
					// Check whether the current superuser password is weak
					// (i.e. whether it is "superuser").
					var err error
					setSuperuserPW, err = s.mgnlCli.IsSuperuserPW(gCtx, superuserName, "superuser")
					if err != nil {
						if errors.Is(err, context.Canceled) {
							return backoff.Permanent(err)
						}
						// Also set the superuser if the current password is not
						// bcrypted.
						if errors.Is(err, magnolia.ErrPasswordNotBcrypted) {
							logrus.Warnf("encountered superuser password which is not bcrypted")
							setSuperuserPW = true
						} else {
							return fmt.Errorf("could not check superuser password: %w", err)
						}
					}
					return nil
				},
			),
			bo,
		); err != nil {
			return err
		}

		// If the password is weak, we set it to the password given in the
		// secret. Otherwise we keep the password we already have and delete the
		// secret.
		if setSuperuserPW {
			// Set superuser password with retry.
			g.Go(func() error {
				return backoff.Retry(
					errorLogWrapper(
						"setting superuser password",
						func() error {
							err := s.mgnlCli.SetSuperuserPw(gCtx, superuserName, string(pw))
							if err != nil {
								if errors.Is(err, context.Canceled) {
									return backoff.Permanent(err)
								}
							}
							return err
						},
					),
					bo,
				)
			})
		} else {
			logrus.Infof("not setting superuser password: current password is considered to be strong enough")
			// If the superuser password in the secret is not matching the
			// password that is set in magnolia, ...
			isPWMatching, err := s.mgnlCli.IsSuperuserPW(gCtx, superuserName, string(pw))
			if err != nil {
				return fmt.Errorf("could not check superuser password: %w", err)
			}
			if !isPWMatching {
				// ... delete the secret containing the superuser password (to
				// prevent confusion).
				if err := s.k8sCli.cliSet.CoreV1().Secrets(s.k8sCli.namespaceName).Delete(s.ctx, superuserSecretName, metav1.DeleteOptions{}); err != nil {
					return fmt.Errorf("could not delete secret: %w", err)
				}
				logrus.Infof("deleted secret %s because the superuser password it contained was not matching the actual superuser password", superuserSecretName)
			}
		}
		return nil
	})

	return nil
}

type resulter interface {
	Result() health.Result
}

func statusHandler(check resulter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var buf bytes.Buffer
		msg := check.Result()
		if err := json.NewEncoder(&buf).Encode(msg); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			logrus.Error(err)
			return
		}
		w.Header().Set("Content-Type", health.ContentType)
		if msg.Status != health.StatusPass {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
		if _, err := io.Copy(w, &buf); err != nil {
			logrus.Errorf("error sending error to client: %s", err)
		}
	}
}

// Deprecated. Here for backwards compatibility. Use /readyz and /livez instead.
func (s *server) handleHealthz() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
}

func (s *server) handleLivez() http.HandlerFunc {
	return statusHandler(s.livenessCheck)
}

func (s *server) handleReadyz() http.HandlerFunc {
	return statusHandler(s.readinessCheck)
}

func (s *server) handleStartz() http.HandlerFunc {
	return statusHandler(s.livenessCheck)
}

func (s *server) handleReenableSuperuser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), handleSuperuserPasswordTimeout)
		defer cancel()
		if err := r.ParseForm(); err != nil {
			logrus.Error(err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		user := r.Form.Get(urlQueryParamUser)
		if user == "" {
			err := fmt.Errorf("parameter %s must be set", urlQueryParamUser)
			logrus.Error(err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		if err := s.mgnlCli.ReenableSuperuser(ctx, user); err != nil {
			if errors.Is(err, magnolia.ErrSuperuserSemaphorTaken) {
				sendError(w, err, http.StatusTooManyRequests)
				return
			}
			logrus.Error(err)
			sendError(w, fmt.Errorf("could not reenable superuser"), http.StatusInternalServerError)
		}
	}
}

func (s *server) handleSuperuserPassword() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), handleSuperuserPasswordTimeout)
		defer cancel()
		if err := r.ParseForm(); err != nil {
			logrus.Error(err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		user := r.Form.Get(urlQueryParamUser)
		if user == "" {
			err := fmt.Errorf("parameter %s must be set", urlQueryParamUser)
			logrus.Error(err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		pw := r.Form.Get(urlQueryParamPW)
		if pw == "" {
			err := fmt.Errorf("parameter %s must be set", urlQueryParamPW)
			logrus.Error(err)
			sendError(w, err, http.StatusBadRequest)
			return
		}
		if err := s.mgnlCli.SetSuperuserPw(ctx, user, pw); err != nil {
			if errors.Is(err, magnolia.ErrSuperuserSemaphorTaken) {
				sendError(w, err, http.StatusTooManyRequests)
				return
			}
			sendError(w, fmt.Errorf("could not set superuser"), http.StatusInternalServerError)
		}
	}
}

func (s *server) handleSyncActivationKey() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		select {
		case <-s.ctx.Done():
			return
		case s.activationSync.syncKey <- struct{}{}:
			logrus.Debug("triggered manual activation key sync")
		}
	}
}

func sendError(w http.ResponseWriter, err error, status int) {
	var exp interface{}
	if err, ok := err.(interface{ ExpandedMsg() interface{} }); ok {
		exp = err.ExpandedMsg()
	}
	var msg = struct {
		Status      string      `json:"status,omitempty"`
		StatusCode  int         `json:"status_code,omitempty"`
		Msg         string      `json:"msg,omitempty"`
		ExpandedMsg interface{} `json:"expanded_msg,omitempty"`
	}{
		Status:      http.StatusText(status),
		StatusCode:  status,
		Msg:         err.Error(),
		ExpandedMsg: exp,
	}
	w.Header().Set("Content-type", "application/json")
	if status == http.StatusTooManyRequests {
		w.Header().Set("Retry-After", "30")
	}
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		logrus.Errorf("could not send error message: %v", err)
	}
}
