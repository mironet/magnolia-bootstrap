package cmd

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"testing"

	jwt "github.com/dgrijalva/jwt-go/v4"
)

func Test_readProperties(t *testing.T) {
	expected := `#generated 09.May.2020 04:55 by superuser
	#Sat May 09 16:55:03 CEST 2020
	key.private=30820277020100300...
	key.public=30819F300D06092A8648...
	`

	commentTrap := `#key.private=schnischna
	key.public=30819F300D06092A8648...`

	type args struct {
		in io.Reader
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]string
		wantErr bool
	}{
		{
			name: "expected input",
			args: args{
				in: strings.NewReader(expected),
			},
			want: map[string]string{
				"key.private": "30820277020100300...",
				"key.public":  "30819F300D06092A8648...",
			},
			wantErr: false,
		},
		{
			name: "no properties input",
			args: args{
				in: strings.NewReader("blablabla"),
			},
			want:    map[string]string{},
			wantErr: false,
		},
		{
			name: "comment trap",
			args: args{
				in: strings.NewReader(commentTrap),
			},
			want: map[string]string{
				"key.public": "30819F300D06092A8648...",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := readProperties(tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("readProperties() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readProperties() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_extractPubKey(t *testing.T) {
	expected := `#generated 09.May.2020 04:55 by superuser
	#Sat May 09 16:55:03 CEST 2020
	key.private=30820277020100300...
	key.public=30819F300D06092A8648...
	`

	commentTrap := `#key.private=schnischna
	key.public=30819F300D06092A8648...`

	type args struct {
		in string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "expected key",
			args: args{in: expected},
			want: "30819F300D06092A8648...",
		},
		{
			name: "comment trap key",
			args: args{in: commentTrap},
			want: "30819F300D06092A8648...",
		},
		{
			name:    "no key",
			args:    args{in: "blablabla"},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := extractPubKey(tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("extractPubKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("extractPubKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_templateThis(t *testing.T) {
	simple := `there's nothing to do here.`
	defaultValues := `test-{{ .Env.MAGNOLIA_ENDPOINT | default "default-endpoint" }}`
	podOrdinal := `<param name="url" value="jdbc:postgresql://database-hostname-{{ .Env.HOSTNAME | podordinal }}:5432/dbname"/>`

	// Use of the the .File object.
	fileContent := `{{ .File.Get "testdata/fixtures/superuser.yml" }}`
	wantFileContent := fixture("superuser.yml", t)

	// Try to generate a JWT with a private key in a file.
	jwtTemplate := `{{ genjwt "{ \"name\": \"test\" }" (.File.Get "testdata/fixtures/test-private-key.pem") }}`
	wantJwt := `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoidGVzdCJ9.sYPvVnn9k3xuyPmOotI8mLyHbzgFGzc78IBb0GuyNzak9zudLx20Pr9UlvSkYVZSgouVj3zpBVN_JjgtOovzK034BJEOv37TW1c8X90WJbnlowgM_zEWOKm7hKCzUZtUO7lwZRsPSaYezDght_N_F5DXhwAJHowaiFuzrhH3CWYwXG5Wtoc5iZ3EVuQQ9K1s_cZDM5nzFqbOFQh0EWdbkmnA2hyG29wvklEe6hP3ytlnhp9hcDIjE39vlEGLvYs2j4WHERByXzDe1uTselLNKeDj6NW8ZoJE4rnZTsAMmh5osQA0-3K3CA4ApD8fwqyeyV5v_LpnmKhojs1U7GzchQ`

	publicKeyTpl := `{{ pkcs1PubKey (.File.Get "testdata/fixtures/test-private-key.pem") }}`
	wantPublicKey := fixture("test-public-key.pem", t)

	type args struct {
		in io.Reader
	}
	tests := []struct {
		name    string
		args    args
		env     map[string]string
		wantOut string
		wantErr bool
	}{
		{
			name:    "simple",
			args:    args{in: strings.NewReader(simple)},
			wantOut: simple,
			wantErr: false,
		},
		{
			name:    "defaults",
			args:    args{in: strings.NewReader(defaultValues)},
			wantOut: "test-default-endpoint",
			wantErr: false,
		},
		{
			name: "replace values",
			args: args{in: strings.NewReader(defaultValues)},
			env: map[string]string{
				"MAGNOLIA_ENDPOINT": "superendpoint",
			},
			wantOut: "test-superendpoint",
			wantErr: false,
		},
		{
			name: "replace values, pod ordinal",
			args: args{in: strings.NewReader(podOrdinal)},
			env: map[string]string{
				"HOSTNAME": "app-hostname-1",
			},
			wantOut: `<param name="url" value="jdbc:postgresql://database-hostname-1:5432/dbname"/>`,
			wantErr: false,
		},
		{
			name:    "file contents",
			args:    args{in: strings.NewReader(fileContent)},
			wantOut: wantFileContent,
			wantErr: false,
		},
		{
			name:    "jwt",
			args:    args{in: strings.NewReader(jwtTemplate)},
			wantOut: wantJwt,
			wantErr: false,
		},
		{
			name:    "pkcs1PubKey",
			args:    args{in: strings.NewReader(publicKeyTpl)},
			wantOut: wantPublicKey,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if len(tt.env) > 0 {
				for k, v := range tt.env {
					os.Setenv(k, v)
					defer os.Unsetenv(k)
				}
			}
			//out := &bytes.Buffer{}
			out := new(strings.Builder)
			if err := templateThis(out, tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("templateThis() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotOut := out.String(); gotOut != tt.wantOut {
				t.Errorf("templateThis() = %s, want %s", gotOut, tt.wantOut)
			}
		})
	}
}

func Test_podOrdinal(t *testing.T) {
	type args struct {
		hostname string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "ordinal",
			args:    args{hostname: "magnolia-public-0"},
			want:    "0",
			wantErr: false,
		},
		{
			name:    "ordinal",
			args:    args{hostname: "magnolia-public-124"},
			want:    "124",
			wantErr: false,
		},
		{
			name:    "ordinal, number in between",
			args:    args{hostname: "magnolia-0091824-public-124"},
			want:    "124",
			wantErr: false,
		},
		{
			name:    "no number at the end",
			args:    args{hostname: "magnolia-0091824-public"},
			want:    "",
			wantErr: true,
		},
		{
			name:    "no number at all",
			args:    args{hostname: "magnolia-public"},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := podOrdinal(tt.args.hostname)
			if (err != nil) != tt.wantErr {
				t.Errorf("podOrdinal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("podOrdinal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_jwtToken(t *testing.T) {
	type wantFunc func(in string) error

	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Error(err)
		return
	}
	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		t.Error(err)
		return
	}
	privateKey := pem.EncodeToMemory(&pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: privBytes,
	})
	pubBytes, err := x509.MarshalPKIXPublicKey(&priv.PublicKey)
	if err != nil {
		t.Error(err)
		return
	}
	publicKey := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pubBytes,
	})

	verifyToken := func(in string) error {
		token, err := jwt.Parse(in, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			// We return the public key here to verify.
			return &priv.PublicKey, nil
		})
		if err != nil {
			return fmt.Errorf("error verifying token: %w", err)
		}
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			t.Logf("%+v", claims)
		}
		t.Logf("token: %v", token)
		return nil
	}

	t.Logf("testing with private key:\n%s", string(privateKey))
	t.Logf("testing with public key:\n%s", string(publicKey))

	type args struct {
		payload []byte
		pem     []byte
	}
	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "normal signing",
			args: args{
				payload: []byte(`{ "user": "superuser", "role": "superuser" }`),
				pem:     privateKey,
			},
			want: verifyToken,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := jwtToken(tt.args.payload, tt.args.pem)
			if (err != nil) != tt.wantErr {
				t.Errorf("jwtToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want == nil {
				return
			}
			if err := tt.want(got); err != nil {
				t.Errorf("jwtToken() error = %v", err)
				return
			}
		})
	}
}

func Test_pkcs1PubKey(t *testing.T) {
	privatKeyStr := fixture("test-private-key.pem", t)
	publicKeyStr := fixture("test-public-key.pem", t)

	type args struct {
		private string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "pkcs1",
			args: args{
				private: privatKeyStr,
			},
			want:    publicKeyStr,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pkcs1PubKey(tt.args.private)
			if (err != nil) != tt.wantErr {
				t.Errorf("pkcs1PubKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("pkcs1PubKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
