package cmd

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
)

func generateKeyPair() ([]byte, []byte, error) {
	// Generate private key.
	privatekey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, fmt.Errorf("could not generate RSA key: %w", err)

	}
	publickey := &privatekey.PublicKey

	// Encode private key to pem bytes.
	var privateKeyBytes []byte = x509.MarshalPKCS1PrivateKey(privatekey)
	privateKeyBlock := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: privateKeyBytes,
	}
	privatePem := pem.EncodeToMemory(privateKeyBlock)
	if privatePem == nil {
		return nil, nil, fmt.Errorf("error encoding private pem")
	}

	// Encode public key to pem bytes.
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(publickey)
	if err != nil {
		return nil, nil, fmt.Errorf("error when dumping publickey: %w", err)
	}
	publicKeyBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: publicKeyBytes,
	}
	publicPem := pem.EncodeToMemory(publicKeyBlock)
	if publicPem == nil {
		return nil, nil, fmt.Errorf("error encoding public pem")
	}
	return privatePem, publicPem, nil
}

// genActivationKeyPair does what is documented here
// https://gitlab.com/mironet/magnolia-helm#direct-key-input.
func genActivationKeyPair(size int) (private, public string, err error) {
	// Generate rsa private key.
	privatekey, err := rsa.GenerateKey(rand.Reader, size)
	if err != nil {
		return "", "", fmt.Errorf("could not generate RSA key: %w", err)

	}
	publickey := &privatekey.PublicKey

	// Encode private key to der bytes.
	privateDer, err := x509.MarshalPKCS8PrivateKey(privatekey)
	if err != nil {
		return "", "", fmt.Errorf("could not marshal pkcs8 private key: %w", err)
	}

	// Encode public key to der bytes.
	publicDer, err := x509.MarshalPKIXPublicKey(publickey)
	if err != nil {
		return "", "", fmt.Errorf("error when dumping publickey: %w", err)
	}

	return hex.EncodeToString(privateDer), hex.EncodeToString(publicDer), nil
}

// keyFromHex turns the Magnolia hex-encoded key format and transforms it into
// a private key object. Only the private key is needed, the public key can
// always be derived from the private key.
func keyFromHex(private string) (crypto.PrivateKey, error) {
	privateDer, err := hex.DecodeString(private)
	if err != nil {
		return nil, fmt.Errorf("could not hex decode private key: %w", err)
	}
	key, err := x509.ParsePKCS8PrivateKey(privateDer) // We know it's PKCS8.
	if err != nil {
		return nil, fmt.Errorf("could not parse private key DER encoded bytes (PKCS8): %w", err)
	}
	return key, nil
}

// pubkeyFromHex does the same as keyFromHex but for public keys.
func pubkeyFromHex(public string) (crypto.PublicKey, error) {
	publicDer, err := hex.DecodeString(public)
	if err != nil {
		return nil, fmt.Errorf("could not hex decode public key: %w", err)
	}
	key, err := x509.ParsePKIXPublicKey(publicDer) // We know it's PKIX.
	if err != nil {
		return nil, fmt.Errorf("could not parse public key DER encoded bytes (PKIX): %w", err)
	}
	return key, nil
}
