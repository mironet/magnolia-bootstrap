# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [v0.11.0] - 2025-01-27

[MR-44](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/44):

### Changed

- Set the receivers config only in author's microprofile.

## [v0.10.0] - 2024-10-16

[MR-43](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/43):

### Added

- Add startup and own liveness probe endpoint.

### Changed

- Upgrade to go1.23.

## [v0.9.1] - 2024-08-28

### Fixed

[MR-41](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/41):

- Update kaniko version to make the built images OCI-compliant.

## [v0.9.0] - 2024-08-26

### Added

[MR-38](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/38):

- Add support for Magnolia 6.3 (Microprofiles, Password app key handling).

## [v0.8.3] - 2024-07-31

### Fixed

Both fixes have been introduced by
[MR-39](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/39):

- Need to sort subset addresses to ensure that we are consistent over different
  runs of the receiver reconciliation.
- When checking whether a receiver node must be added, be more thorough, i.e.
  compare not only names but also properties.

## [v0.8.2] - 2024-05-21

### Fixed

- [MR-36](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/36) Do
  not display bearer token in (trace mode) log.

## [v0.8.1] - 2024-04-25

### Changed

All changes have been introduced by
[MR-33](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/33):

- Receiver reconciliation: Buffer incoming events instead of discarding them.
- For `Deleted` events sleep for 5 seconds before reconciling to give the deleted
  resource a chance to reappear again before reconciliation.
- Don't use deprecated package `io/ioutil`.
- Just in case: Update modules to fix possible vulnerabilities
  ([GO-2024-2687](https://pkg.go.dev/vuln/GO-2024-2687) &
  [GO-2023-1631](https://pkg.go.dev/vuln/GO-2023-1631)).

## [v0.8.0] - 2024-03-04

### Added

- [MR-32](https://gitlab.com/mironet/magnolia-bootstrap/-/merge_requests/32)
  Change public receivers registration: Use endpoints (instead of pods) for
  "local" public registration & service (instead of endpoints) for "remote"
  public registration.
- Add job "build-app-manually" to gitlab pipeline.

### Changed

- Perform refactors/fixes proposed by golangci-lint.
