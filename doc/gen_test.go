package main

import (
	"reflect"
	"testing"
)

func Test_parseTag(t *testing.T) {
	type args struct {
		in string
	}
	tests := []struct {
		name    string
		args    args
		want    tag
		wantErr bool
	}{
		{
			name: "yaml, omitempty",
			args: args{in: "`yaml:\"endpoints,omitempty\"`"},
			want: tag{
				Name:     "endpoints",
				Required: false,
			},
		},
		{
			name: "yaml, required",
			args: args{in: "`yaml:\"name\"`"},
			want: tag{
				Name:     "name",
				Required: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseTag(tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseTag() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseTag() = %v, want %v", got, tt.want)
			}
		})
	}
}
