package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"strings"
	"unicode"

	"text/template"
)

const (
	// Called from one level above.
	sourceFile = "doc/_README.md"
	destFile   = "README.md"
)

const instrTpl = `{{ define "instructions" -}}
{{- range . -}}
#### {{ .Name }}

| Name | Usage |
|-|-|
{{- range .Fields }}
| ` + "`{{ .Name }}`" + ` | {{ .Comment }} |
{{- end }}

{{ end -}}
{{- end -}}`

type structs struct {
	Name   string
	Fields []sfield
}

type sfield struct {
	Name    string
	Comment string
}

func main() {
	tplsrc, err := os.ReadFile(sourceFile)
	if err != nil {
		log.Print(err)
		return
	}

	tpl := template.New("readme")
	tpli := tpl.New("instructions")
	tpl, err = tpl.Parse(string(tplsrc))
	if err != nil {
		log.Print(err)
		return
	}
	_, err = tpli.Parse(string(instrTpl))
	if err != nil {
		log.Print(err)
		return
	}

	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "cmd/instructions.go", nil, parser.ParseComments)
	if err != nil {
		log.Print(err)
		return
	}

	var data = make([]structs, 0)
	var i int = -1
	ast.Inspect(f, func(n ast.Node) bool {
		if n == nil {
			return false
		}
		switch t := n.(type) {
		case *ast.TypeSpec:
			name := t.Name.String()
			r := rune(name[0])
			if !unicode.IsUpper(r) && unicode.IsLetter(r) {
				return true
			}
			data = append(data, structs{
				Name:   name,
				Fields: make([]sfield, 0),
			})
			i++
		case *ast.StructType:
			for _, field := range t.Fields.List {
				t, err := parseTag(field.Tag.Value)
				if err != nil {
					log.Print(err)
					return false
				}
				data[i].Fields = append(data[i].Fields, sfield{
					Name:    t.Name,
					Comment: strings.TrimSpace(field.Comment.Text()),
				})
			}
		}
		return true
	})

	dest, err := os.Create(destFile)
	if err != nil {
		log.Print(err)
		return
	}
	defer dest.Close()

	if err := tpl.Execute(dest, data); err != nil {
		log.Print(err)
		return
	}
}

type tag struct {
	Name     string
	Required bool
}

func parseTag(in string) (tag, error) {
	str := strings.Trim(in, "`")
	parts := strings.Split(str, `:`)
	if len(parts) != 2 {
		return tag{}, fmt.Errorf("%s does not look like a tag def", in)
	}
	if parts[0] != "yaml" {
		return tag{}, fmt.Errorf("not implemented")
	}
	parts[1] = strings.Trim(parts[1], `"`)
	parts = strings.Split(parts[1], ",")
	if len(parts) > 2 {
		return tag{}, fmt.Errorf("not implemented")
	}
	t := tag{
		Name: parts[0],
	}
	if len(parts) < 2 {
		t.Required = true
	}
	if len(parts) > 1 && parts[1] == "omitempty" {
		t.Required = false
	}
	return t, nil
}
